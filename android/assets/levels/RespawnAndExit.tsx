<?xml version="1.0" encoding="UTF-8"?>
<tileset name="RespawnAndExit" tilewidth="100" tileheight="100">
 <image source="resources/RespawnAndExit.png" width="200" height="100"/>
 <tile id="0">
  <properties>
   <property name="respawn" value="true"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="exit" value="true"/>
  </properties>
 </tile>
</tileset>
