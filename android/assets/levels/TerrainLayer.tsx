<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Terrain Layer" tilewidth="100" tileheight="100">
 <image source="resources/meadow.png" width="512" height="256"/>
 <tile id="0">
  <properties>
   <property name="down" value="false"/>
   <property name="left" value="false"/>
   <property name="right" value="true"/>
   <property name="through_road" value="true"/>
   <property name="up" value="true"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="down" value="true"/>
   <property name="left" value="false"/>
   <property name="right" value="true"/>
   <property name="through_road" value="true"/>
   <property name="up" value="false"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="through_road" value="true"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="through_road" value="true"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="through_road" value="true"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="down" value="false"/>
   <property name="left" value="true"/>
   <property name="right" value="false"/>
   <property name="through_road" value="true"/>
   <property name="up" value="true"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="down" value="true"/>
   <property name="left" value="true"/>
   <property name="right" value="false"/>
   <property name="through_road" value="true"/>
   <property name="up" value="false"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="through_road" value="false"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="through_road" value="true"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="through_road" value="true"/>
  </properties>
 </tile>
</tileset>
