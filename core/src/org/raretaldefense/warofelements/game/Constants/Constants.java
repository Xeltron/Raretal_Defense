package org.raretaldefense.warofelements.game.Constants;

import com.badlogic.gdx.Gdx;

public final class Constants {


    //==================================
    //          DEBUG
    //==================================

    // This variable indicate if is necessary to generate a new Atlas
    public final static boolean rebuildAtlasGame = false;
    public final static boolean rebuildAtlasMenu = false;

    //If true, the imagesOfCanons build with Atlas will have borderlines to represent the padding between them
    public final static boolean linesDebugAtlas = false;

    public final static boolean runHiero = false;

    //==================================
    //          SCREEN
    //==================================


    //This is the relation middle approximate of the width and height of screen. If other value is bigger than this,
    // it means that the screen is very large. Otherwise, it means that the dimensions of screens are approaching to square.
    public final static float FRONTEIR_TYPES_OF_SCREEN = 1.5f;


    //==================================
    //          CAMERA
    //==================================

    public static float unitScale = 1 / 100f;
    public final static float MAX_ZOOM_IN = 0.25f;
    public final static float MAX_ZOOM_OUT = 10f;


    //==================================
    //          PATHS
    //==================================

    //Variables for build TextureAtlas
    public final static String PATH_ORIGIN_ATLAS = "images";
    public final static String PATH_DESTINATION_ATLAS = "images";
    public final static String NAME_PACKAGE_ATLAS = "raretalDefense.pack";
    public final static String PATH_ORIGIN_ATLAS_MENU = "screens";
    public final static String PATH_DESTINATION_ATLAS_MENU = "screens";
    public final static String NAME_PACKAGE_ATLAS_MENU = "raretalDefense_menu.pack";

    //Path of resources for Android and Linux
    public static final String TEXTURE_ATLAS_MENU_OBJECTS = "screens/raretalDefense_menu.pack.atlas";
    public static final String TEXTURE_ATLAS_OBJECTS = "images/raretalDefense.pack.atlas";
    public final static String PATH_LEVELS = "levels/level";
    public final static String PATH_FONTS = "fonts";

    //Path of resources for Windows

    public static final String PATH_ORIGIN_ATLAS_DESKTOP = "android/assets/" + PATH_ORIGIN_ATLAS;
    public static final String PATH_DESTINATION_ATLAS_DESKTOP = "android/assets/" + PATH_DESTINATION_ATLAS;
    public static final String TEXTURE_ATLAS_MENU_OBJECTS_DESKTOP = Gdx.files.getLocalStoragePath().replace("\\", "/") + "screens/raretalDefense_menu.pack.atlas";
    public static final String TEXTURE_ATLAS_OBJECTS_DESKTOP = Gdx.files.getLocalStoragePath().replace("\\", "/") + "images/raretalDefense.pack.atlas";
    public final static String PATH_LEVELS_DESKTOP = Gdx.files.getLocalStoragePath().replace("\\", "/") + "levels/level";
    public final static String PATH_FONTS_DESKTOP = Gdx.files.getLocalStoragePath().replace("\\", "/") + "fonts";


    public final static String FONT_CALADEA_LITTLE = "caladea-18.fnt";
    public final static String FONT_CALADEA_MEDIUM = "caladea-24.fnt";
    public final static String FONT_CALADEA_BIG = "caladea-32.fnt";

    public final static String BACKGROUND_GENERAL = "";
    public final static String JSON_ITEMS_UI_SKIN = "screens/uiskin.json";
    public final static String LOGINSCREEN_TABLE_BACKGROUND = "screens/login_frame.png";
    public final static String MENUSCREEN_TABLE_BACKGROUND = "screens/menu_panel_buttons.png";
    public final static String MENUSCREEN_SIGNOUT_BTN = "screens/sign_out.png";


    //==================================
    //          TILEDMAP
    //==================================

    //Layers
    public final static String TILEDMAP_TERRAIN_LAYER = "Terrain Layer";
    public final static String TILEDMAP_RESPAWN_EXIT_LAYER = "RespawnAndExit";

    //Attributes of TiledMap
    public final static String TILEDMAP_WIDTH = "width";
    public final static String TILEDMAP_HEIGHT = "height";
    public final static String TILEDMAP_TILEWIDTH = "tilewidth";
    public final static String TILEDMAP_TILEHEIGHT = "tileheight";
    public final static String TILEDMAP_ROUNDS = "rounds";
    public final static String TILEDMAP_FIRST_ENEMIES = "initEnemies";
    public final static String TILEDMAP_FIRST_COINS = "started_coins";
    public final static String TILEDMAP_PENALTY_HEALTH = "health_penalty";
    public final static String TILEDMAP_STAR_FIRST = "star_first";
    public final static String TILEDMAP_STAR_SECOND = "star_second";
    public final static String TILEDMAP_STAR_THIRD = "star_third";

    //Attributes of TiledMapTileLayer
    public final static String TILEDMAP_ATTRIBUTE_THROUGH_ROAD = "through_road";
    public final static String TILEDMAP_ATTRIBUTE_UP = "up";
    public final static String TILEDMAP_ATTRIBUTE_DOWN = "down";
    public final static String TILEDMAP_ATTRIBUTE_LEFT = "left";
    public final static String TILEDMAP_ATTRIBUTE_RIGHT = "right";
    public final static String TILEDMAP_ATTRIBUTE_RESPAWN = "respawn";
    public final static String TILEDMAP_ATTRIBUTE_EXIT = "exit";


    //Size Cell
    public static int SIZE_CELL_WIDTH;
    public static int SIZE_CELL_HEIGHT;


    //==================================
    //           ENEMIES
    //==================================

    // GLOBAL
    public final static int SCORE_ENEMY = 50;
    public final static int COINS_ENEMY = 15;
    public final static float VELOCITIY_BONUS_CHANGE = 1.5f;

    public enum STATE_ENEMY {
        NORMAL, SLOW, FAST, CONFUSED, SLOW_CONFUSED, FAST_CONFUSED;
    }

    // FIRE ENEMY
    public final static float VELOCITY_BASE_FIRE_ENEMY = 100f;
    public final static int HEALTH_FIRE_ENEMY = 80;
    public final static int DAMAGE_FIRE_ENEMY = 2;
    public final static String REFERENCE_NAME_ATLAS_FIRE_ENEMY = "Enemy_Red";


    // NORMAL ENEMY
    public final static float VELOCITY_BASE_NORMAL_ENEMY = 100;
    public final static int HEALTH_NORMAL_ENEMY = 130;
    public final static int DAMAGE_NORMAL_ENEMY = 1;
    public final static String REFERENCE_NAME_ATLAS_NORMAL_ENEMY = "Enemy_Green";


    // WATER ENEMY
    public final static float VELOCITY_BASE_WATER_ENEMY = 120f;
    public final static int HEALTH_WATER_ENEMY = 80;
    public final static int DAMAGE_WATER_ENEMY = 1;
    public final static String REFERENCE_NAME_ATLAS_WATER_ENEMY = "Enemy_Blue";


    //==================================
    //             TOWERS
    //==================================

    // FIRE TOWER
    public final static float[] VELOCITY_ATTACK_FIRE_TOWER = {0.04f};
    public final static int[] VALUE_FIRE_TOWER = {100, 250, 500};
    public final static int[] DAMAGE_FIRE_TOWER = {2, 4, 8};
    public final static int[] AREA_ATTACK_FIRE_TOWER = {2};
    public final static String REFERENCE_NAME_ATLAS_FIRE_TOWER = "TowerFire";
    public final static String REFERENCE_NAME_ATLAS_BASE_FIRE_TOWER = "base_tower_fire";


    // WATER TOWER
    public final static float[] VELOCITY_ATTACK_WATER_TOWER = {0.08f};
    public final static int[] VALUE_WATER_TOWER = {100, 250, 500};
    public final static int[] DAMAGE_WATER_TOWER = {2, 6, 15};
    public final static int[] AREA_ATTACK_WATER_TOWER = {2, 3, 4};
    public final static String REFERENCE_NAME_ATLAS_WATER_TOWER = "TowerWater";
    public final static String REFERENCE_NAME_ATLAS_BASE_WATER_TOWER = "base_tower_water";


    // WIND TOWER
    public final static float[] VELOCITY_ATTACK_WIND_TOWER = {0.07f};
    public final static int[] VALUE_WIND_TOWER = {50, 75, 100};
    public final static int[] DAMAGE_WIND_TOWER = {0};
    public final static int[] AREA_ATTACK_WIND_TOWER = {2, 3, 4};
    public final static String REFERENCE_NAME_ATLAS_WIND_TOWER = "TowerWind";
    public final static String REFERENCE_NAME_ATLAS_BASE_WIND_TOWER = "base_tower_wind";


    // THUNDER TOWER
    public final static float[] VELOCITY_ATTACK_THUNDER_TOWER = {0.07f, 0.04f, 0.02f};
    public final static int[] VALUE_THUNDER_TOWER = {100, 250, 500};
    public final static int[] DAMAGE_THUNDER_TOWER = {3};
    public final static int[] AREA_ATTACK_THUNDER_TOWER = {3, 4, 5};
    public final static String REFERENCE_NAME_ATLAS_THUNDER_TOWER = "TowerThunder";
    public final static String REFERENCE_NAME_ATLAS_BASE_THUNDER_TOWER = "base_tower_thunder";


    //==================================
    //           PROJECTILES
    //==================================

    // FIRE PROJECTILE
    public final static int VELOCITY_BASE_FIRE_PROJECTILE = 200;
    public final static String REFERENCE_NAME_ATLAS_FIRE_PROJECTILE = "FireProjectile";


    // WATER PROJECTILE
    public final static int VELOCITY_BASE_WATER_PROJECTILE = 200;
    public final static String REFERENCE_NAME_ATLAS_WATER_PROJECTILE = "WaterProjectile";


    // WIND PROJECTILE
    public final static int VELOCITY_BASE_WIND_PROJECTILE = 200;
    public final static String REFERENCE_NAME_ATLAS_WIND_PROJECTILE = "WindProjectile";


    // THUNDER PROJECTILE
    public final static int VELOCITY_BASE_THUNDER_PROJECTILE = 400;
    public final static String REFERENCE_NAME_ATLAS_THUNDER_PROJECTILE = "ThunderProjectile";


    // ROCK PROJECTILE
    public final static int VELOCITY_BASE_ROCK_PROJECTILE = 400;
    public final static String REFERENCE_NAME_ATLAS_ROCK_PROJECTILE = "Trap_Rocks";


    //==================================
    //             TRAPS
    //==================================

    // CONFUSION TRAP
    public final static float[] COOLDOWN_CONFUSUION_TRAP = {15f, 12f, 10f};
    public final static float[] COOLDOWN_STATUS_ACTIVE_CONFUSION_TRAP = {3f, 3f, 3f};
    public final static int[] DAMAGE_CONFUSION_TRAP = {0};
    public final static int[] VALUE_CONFUSION_TRAP = {200, 400, 500};
    public final static String REFERENCE_NAME_ATLAS_CONFUSION_TRAP = "ConfusionTrap";
    public final static String REFERENCE_NAME_ATLAS_CONFUSION_TRAP_ACTIVATE = "ConfusionTrap_Activate";


    // ROCK TRAP
    public final static float[] COOLDOWN_ROCK_TRAP = {7f, 12f, 10f};
    public final static float[] COOLDOWN_STATUS_ACTIVE_ROCK_TRAP = {0f};
    public final static int[] DAMAGE_ROCK_TRAP = {200};
    public final static int[] VALUE_ROCK_TRAP = {100, 400, 500};
    public final static String REFERENCE_NAME_ATLAS_ROCK_TRAP = "RockTrap";


    // TRAPDOOR
    public final static float[] COOLDOWN_TRAPDOOR = {20f, 12f, 10f};
    public final static float[] COOLDOWN_STATUS_ACTIVE_TRAPDOOR = {1.5f, 3f, 3f};
    public final static int[] DAMAGE_TRAPDOOR = {0};
    public final static int[] VALUE_TRAPDOOR = {300, 500, 700};
    public final static String REFERENCE_NAME_ATLAS_TRAPDOOR_CLOSE = "TrapdoorClose";
    public final static String REFERENCE_NAME_ATLAS_TRAPDOOR_OPEN = "TrapdoorOpen";


    // COOLDOWN
    public final static String REFERENCE_NAME_ATLAS_COOLDOWN_TRAPS = "CooldownTrap";


    //==================================
    //           GAME OBJECTS
    //==================================

    // ENEMIES
    public final static int NORMAL_ENEMY = 0;
    public final static int WATER_ENEMY = 1;
    public final static int FIRE_ENEMY = 2;

    // TOWERS
    public final static String FIRE_TOWER = "fireTower";
    public final static String THUNDER_TOWER = "thunderTower";
    public final static String WATER_TOWER = "waterTower";
    public final static String WIND_TOWER = "windTower";


    // TRAPS
    public final static String CONFUSION_TRAP = "confusionTrap";
    public final static String ROCK_TRAP = "rockTrap";
    public final static String TRAPDOOR = "trapdoor";

    // ELEMENTS
    public final static String FIRE_ELEMENT = "fire";
    public final static String WIND_ELEMENT = "wind";
    public final static String WATER_ELEMENT = "water";
    public final static String THUNDER_ELEMENT = "thunder";


    //==================================
    //          GUI
    //==================================

    public final static String REFERENCE_NAME_ATLAS_HEART = "heart";
    public final static String REFERENCE_NAME_ATLAS_COIN = "coin";
    public final static String REFERENCE_NAME_ATLAS_FIRE_ORB = "FireOrb";
    public final static String REFERENCE_NAME_ATLAS_WATER_ORB = "WaterOrb";
    public final static String REFERENCE_NAME_ATLAS_THUNDER_ORB = "LightingOrb";
    public final static String REFERENCE_NAME_ATLAS_WIND_ORB = "WindOrb";
    public final static String REFERENCE_NAME_ATLAS_ROCK_TRAP_ORB = "Trap_Rock_Orb";
    public final static String REFERENCE_NAME_ATLAS_CONFUSION_TRAP_ORB = "Trap_Confusion_Orb";
    public final static String REFERENCE_NAME_ATLAS_WOOD_PANEL = "wood_panel";
    public final static String REFERENCE_NAME_ATLAS_WOOD_SIGN = "wood_sign";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_HIDE_PANEL_CONSTRUCTION = "button_hide_panel_construction";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_CONSTRUCTION_UP = "button_construction";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_CONSTRUCTION_DOWN = "button_construction_down";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_EVOLVE_UP = "button_evolve_up";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_EVOLVE_DOWN = "button_evolve_down";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_SELL_UP = "button_sell_up";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_SELL_DOWN = "button_sell_down";
    public final static String REFERENCE_NAME_ATLAS_DETAIL_DAMAGE = "damage_detail";
    public final static String REFERENCE_NAME_ATLAS_DETAIL_CONFUSION = "confusion_detail";
    public final static String REFERENCE_NAME_ATLAS_DETAIL_RANGE = "bow_detail";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_START_ROUND_UP = "button_start_round_up";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_SHOW_PANEL_CONSTRUCTION = "button_show_panel_construction";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_PAUSE = "button_pause";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_CANCEL = "button_cancel";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_START_ROUND_DOWN = "button_start_round_down";
    public final static String REFERENCE_NAME_ATLAS_FILTER_BLACK = "filter_black";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_RESUME_UP = "button_resume_up";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_RESUME_DOWN = "button_resume_down";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_EXIT_UP = "button_exit_up";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_EXIT_DOWN = "button_exit_down";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_BACK_MENU_UP = "button_back_menu_up";
    public final static String REFERENCE_NAME_ATLAS_BUTTON_BACK_MENU_DOWN = "button_back_menu_down";
    public final static String REFERENCE_NAME_ATLAS_STAR_ON = "star_on";
    public final static String REFERENCE_NAME_ATLAS_STAR_OFF = "star_off";
    public final static String REFERENCE_NAME_ATLAS_GAME = "game";
    public final static String REFERENCE_NAME_ATLAS_OVER = "over";

    public final static int TYPE_DETAIL_DEFAULT_TOWER = 0;
    public final static int TYPE_DETAIL_TOWER_WIND = 1;
    public final static int TYPE_DETAIL_ROCK_TRAP = 2;
    public final static int TYPE_DETAIL_TRAPDOOR = 3;
    public final static int TYPE_DETAIL_CONFUSION_TRAP = 4;

    public final static int LEVEL_TOTAL_LIVES = 10;
    public final static int NUMBER_DIGITS_MONEY = 4;
    public final static int NUMBER_DIGITS_SCORE = 4;
    public final static float VELOCITY_PANELS = 500;
    public final static float PENALTY_VALUE_SELL = .25f;
    public final static String EXISTING_STRUCTURE = "It already exists";

    //==================================
    //          LEVEL
    //==================================

    public final static float LEVEL_FREQUENCY_UNITS = 10f;
    public final static float BONUS_DAMAGE = 2f;
    public final static int LEVEL_TIME_ROUND = 15; //seconds


    //==================================
    //          SCREENS
    //==================================

    //Global
    public final static String MENU_BACKGROUND = "background_menu";


    //==================================
    //          MENU SCREEN
    //==================================

    public final static String MENU_PANEL_BUTTONS = "menu_panel_buttons";
    public final static String MENU_BUTTON_PLAY_UP = "play_up";
    public final static String MENU_BUTTON_PLAY_DOWN = "play_down";
    public final static String MENU_BUTTON_TUTORIAL_UP = "tutorial_up";
    public final static String MENU_BUTTON_TUTORIAL_DOWN = "tutorial_down";
    public final static String MENU_BUTTON_OPTIONS_UP = "options_up";
    public final static String MENU_BUTTON_OPTIONS_DOWN = "options_down";
    public final static String MENU_BUTTON_EXIT_UP = "exit_up";
    public final static String MENU_BUTTON_EXIT_DOWN = "exit_down";


    //==================================
    //      CONFIGURATION SCREEN
    //==================================

    public final static String CONFIGSCREEN_LBL_MUSIC = "MUSIC";
    public final static String CONFIGSCREEN_CHECKBOX_STYLE = "switch-text";
    public final static String CONFIGSCREEN_SLIDER_STYLE = "default-horizontal";
    public final static String CONFIGSCREEN_LBL_AUDIO = "AUDIO";
    public final static String CONFIGSREEN_CHECKBOX_FULLSCREEN_TEXT = "  FULLSCREEN";
    public final static String CONFIGSCREEN_BTN_SAVE = "Save";
    public final static String CONFIGSCREEN_BTN_CLOSE = "Close";

    //==================================
    //      LEVEL SELECT SCREEN
    //==================================

    public final static int SELECT_LEVEL_NUMBER_STARS = 3;
    public final static String SELECT_LEVEL_BUTTON_01_UP = "l1-up";
    public final static String SELECT_LEVEL_BUTTON_01_DOWN = "l1-down";
    public final static String SELECT_LEVEL_BUTTON_02_UP = "l2-up";
    public final static String SELECT_LEVEL_BUTTON_02_DOWN = "l2-down";
    public final static String SELECT_LEVEL_BUTTON_03_UP = "l3-up";
    public final static String SELECT_LEVEL_BUTTON_03_DOWN = "l3-down";
    public final static String SELECT_LEVEL_MAP = "image_level";

    //==================================
    //            TUTORIAL
    //==================================

    public final static String TUTORIAL_SLIDES = "blackBoard";
    public final static String TUTORIAL_BACK = "button_back";
    public final static String TUTORIAL_NEXT = "button_next";

    //==================================
    //           PROFILE
    //==================================

    public final static String PROFILE_PREFERENCES_SCORES = "raretal_scores";
    public final static String PROFILE_STARS_DINAMIC_KEY = "level";
    public final static String PROFILE_STARS_LEVEL01 = "level01";
    public final static String PROFILE_STARS_LEVEL02 = "level02";
    public final static String PROFILE_STARS_LEVEL03 = "level03";


    //==================================
    //           DESKTOP
    //==================================

    public final static String DESKTOP_NAME_GAME = "Raretal Defense";

}
