package org.raretaldefense.warofelements.game.Game.GameObjects;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;


public abstract class AbstractGameObject {

    //==================================
    //           ATTRIBUTES
    //==================================

    protected Vector2 positionTiledMap;
    protected Vector2 positionScreen;
    protected Vector2 dimension;
    protected Vector2 origin;
    protected Vector2 scale;
    protected float rotation;
    protected boolean dead;


    //==================================
    //           CONSTRUCTOR
    //==================================


    /**
     * @param positionTiledMap The cell position in the map (TiledMap) shown in Vector2
     * @param positionScreen   The pixel position on the screen shown in Vector2
     * @param dimension        The dimension of the object shown in Vector2
     * @param origin           The origin of the object shown in Vector2
     * @param scale            The scale of the object shown in Vector2
     * @param rotation         The rotation of the object
     */
    public AbstractGameObject(Vector2 positionTiledMap, Vector2 positionScreen, Vector2 dimension,
                              Vector2 origin, Vector2 scale, float rotation) {

        // Initialize the basic builder
        this.positionTiledMap = positionTiledMap;
        this.positionScreen = positionScreen;
        this.dimension = dimension;
        this.origin = origin;
        this.scale = scale;
        this.rotation = rotation;
        dead = false;
    }


    //==================================
    //        OVERRIDE METHODS
    //==================================


    public abstract void update(float deltaTime);

    public abstract void render(SpriteBatch batch);


    //==================================
    //        GETTERS & SETTERS
    //==================================


    /**
     * @return The cell position in the map (TiledMap) shown in Vector2
     */
    public Vector2 getPositionTiledMap() {
        return positionTiledMap;
    }

    /**
     * @param positionTiledMap The cell position in the map (TiledMap) shown in Vector2
     */
    public void setPositionTiledMap(Vector2 positionTiledMap) {
        this.positionTiledMap = positionTiledMap;
    }

    /**
     * @return The pixels position on the screen shown in Vector2
     */
    public Vector2 getPositionScreen() {
        return positionScreen;
    }

    /**
     * @param positionScreen The pixels position on the screen shown in Vector2
     */
    public void setPositionScreen(Vector2 positionScreen) {
        this.positionScreen = positionScreen;
    }

    /**
     * @return The dimension of the object shown in Vector2
     */
    public Vector2 getDimension() {
        return dimension;
    }

    /**
     * @param dimension The dimension of the object shown in Vector2
     */
    public void setDimension(Vector2 dimension) {
        this.dimension = dimension;
    }

    /**
     * @return The origin of the object shown in Vector2
     */
    public Vector2 getOrigin() {
        return origin;
    }

    /**
     * @param origin The origin of the object shown in Vector2
     */
    public void setOrigin(Vector2 origin) {
        this.origin = origin;
    }

    /**
     * @return The scale of the object shown in Vector2
     */
    public Vector2 getScale() {
        return scale;
    }

    /**
     * @param scale The scale of the object shown in Vector2
     */
    public void setScale(Vector2 scale) {
        this.scale = scale;
    }

    /**
     * @return The rotation of the object
     */
    public float getRotation() {
        return rotation;
    }

    /**
     * @param rotation The rotation of the element
     */
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    /**
     * @return true if the item is dead, if is dead the level class, remove that item
     * and return false if the item isn't dead
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * @param dead true if the item is dead or false isn't dead
     */
    public void setDead(boolean dead) {
        this.dead = dead;
    }
}

