package org.raretaldefense.warofelements.game.Game.GameObjects.Enemies;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Game.GameObjects.AbstractGameObject;
import org.raretaldefense.warofelements.game.Helpers.Assets;
import org.raretaldefense.warofelements.game.Helpers.MapHelper;

import java.util.ArrayList;

import static com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;

public abstract class AbstractEnemy extends AbstractGameObject {


    //==================================
    //           ATTRIBUTES
    //==================================


    protected final String TAG = getClass().getSimpleName();
    // This vector indicates the direction of the enemy depending on the value of x and y if
    // they are positive or negative
    protected Vector2 velocity;
    protected final float velocityBase;
    protected float currentVelocity;
    protected int health;
    protected int damage;
    protected int level;
    protected ArrayList<String> advantages;
    protected ArrayList<String> disadvantages;
    protected float count = 0;
    protected String comeFrom;
    protected AtlasRegion image;
    protected boolean isDeadForStructure;
    protected boolean isConfused;
    protected boolean inverted;
    protected boolean flipX;
    protected Constants.STATE_ENEMY state;
    protected float countState, countStateConfused;

    protected Circle areaEnemy;


    //==================================
    //           CONSTRUCTOR
    //==================================

    /**
     * @param positionTiledMap The cell position in the map (TiledMap) shown in Vector2
     * @param positionScreen   The pixel position on the screen shown in Vector2
     * @param dimension        The dimension of the object shown in Vector2
     * @param origin           The origin of the object shown in Vector2
     * @param scale            The scale of the object shown in Vector2
     * @param rotation         The rotation of the object
     * @param velocityBase     The moving speed of the enemy
     */
    public AbstractEnemy(Vector2 positionTiledMap, Vector2 positionScreen, Vector2 dimension,
                         Vector2 origin, Vector2 scale, float rotation, float velocityBase, AtlasRegion image) {
        super(positionTiledMap, positionScreen, dimension, origin, scale, rotation);

        // Initialize the basic builder
        this.advantages = new ArrayList<String>();
        this.disadvantages = new ArrayList<String>();
        this.level = 1; // Indicates the level of the enemy
        this.velocityBase = velocityBase;
        this.currentVelocity = velocityBase;

        // Assigned the imagesOfCanons for enemy
        this.image = image;
        // We asigned the correct dimensions of enemy
        this.dimension = Assets.instance.dimensionsOfGameObject(image.getRegionWidth(), image.getRegionHeight());
        this.origin = new Vector2(image.getRegionWidth() / 2f, image.getRegionHeight() / 2f);

        isDeadForStructure = false;
        isConfused = false;
        inverted = false;
        flipX = false;
        state = Constants.STATE_ENEMY.NORMAL;

    }

    //==================================
    //        OVERRIDE METHODS
    //==================================

    @Override
    public void update(float deltaTime) {


        //States of enemy
        if (!state.equals(Constants.STATE_ENEMY.NORMAL)) {

            countState += deltaTime;

            //If the effect have 1 second since reapply, then we remove it
            if (countState > 1) {
                state = Constants.STATE_ENEMY.NORMAL;
                currentVelocity = velocityBase;
                countState = 0;

                if (velocity.x == 0) {
                    if (velocity.y > 0) {
                        velocity.y = currentVelocity;
                    } else {
                        velocity.y = -currentVelocity;
                    }
                } else if (velocity.y == 0) {
                    if (velocity.x > 0) {
                        velocity.x = currentVelocity;
                    } else {
                        velocity.x = -currentVelocity;
                    }
                }
            } else {

                if (state.equals(Constants.STATE_ENEMY.FAST)) { //If the effect is move more fast


                    currentVelocity = velocityBase * Constants.VELOCITIY_BONUS_CHANGE;

                    if (velocity.x == 0) {
                        if (velocity.y > 0) {
                            velocity.y = currentVelocity;
                        } else {
                            velocity.y = -currentVelocity;
                        }
                    } else if (velocity.y == 0) {
                        if (velocity.x > 0) {
                            velocity.x = currentVelocity;
                        } else {
                            velocity.x = -currentVelocity;
                        }
                    }

                } else if (state.equals(Constants.STATE_ENEMY.SLOW)) {

                    currentVelocity = velocityBase / Constants.VELOCITIY_BONUS_CHANGE;

                    if (velocity.x == 0) {
                        if (velocity.y > 0) {
                            velocity.y = currentVelocity;
                        } else {
                            velocity.y = -currentVelocity;
                        }
                    } else if (velocity.y == 0) {
                        if (velocity.x > 0) {
                            velocity.x = currentVelocity;
                        } else {
                            velocity.x = -currentVelocity;
                        }
                    }
                }

            }

        }


        if (isConfused) {

            if (!inverted) {
                velocity.x = -(velocity.x);
                velocity.y = -(velocity.y);

                calculateDirectionIfEnemyIsConfused();

                inverted = true;
            }

            countStateConfused += deltaTime;

            if (countStateConfused > 5) {
                countStateConfused = 0;
                count = 0;
                isConfused = false;
                inverted = false;

                calculateDirectionIfEnemyIsConfused();
            }

        }

        calculatePosition(deltaTime);

        //Gdx.app.debug(TAG,"Position Tiled: "+positionTiledMap.x+" - "+positionTiledMap.y+"  Position Screen: "+positionScreen.x+" - "+positionScreen.y+" Count: "+count);

        if (isNewCell()) {
            if (!isTheLastCell()) {
                // Obtain the map cell to access its properties
                Cell cell = MapHelper.instance.mapCells.get(positionTiledMap);
                if (cell != null) {

                    // Check where is the enemy and where you can go
                    if (MapHelper.instance.canDown(cell) && !comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_DOWN)) {
                        velocity.x = 0;
                        velocity.y = -currentVelocity;
                        rotation = 270;
                    } else if (MapHelper.instance.canUp(cell) && !comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_UP)) {
                        velocity.x = 0;
                        velocity.y = currentVelocity;
                        rotation = 90;
                    } else if (MapHelper.instance.canLeft(cell) && !comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_LEFT)) {
                        velocity.x = -currentVelocity;
                        velocity.y = 0;
                        rotation = 180;
                        flipX = false;
                    } else if (MapHelper.instance.canRight(cell) && !comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_RIGHT)) {
                        velocity.x = currentVelocity;
                        velocity.y = 0;
                        rotation = 0;
                        flipX = true;
                    }
                }
            } else {
                // If the last cell indicate that the enemy is dead
                setDead(true);
            }

        }


    }

    @Override
    public void render(SpriteBatch batch) {

        // Painted the enemy
        batch.draw(image.getTexture(), positionScreen.x, positionScreen.y, origin.x, origin.y,
                dimension.x, dimension.y, 1, 1, 0,
                image.getRegionX(), image.getRegionY(), image.getRegionWidth(), image.getRegionHeight(),
                flipX, false);


    }


    //==================================
    //             METHODS
    //==================================


    /**
     * Method for calculate the direction initial of enemy called from the constructor
     */
    protected void init() {

        // Calculate the direction initial of enemy
        // Look at the cell where the enemy comes to know the direction you have to take
        if (positionTiledMap.x == 0) {
            velocity.x = currentVelocity;
            rotation = 0;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_LEFT;
            flipX = true;
        } else if (positionTiledMap.x == MapHelper.instance.getNumberCellsWidth()) {
            velocity.x = -currentVelocity;
            rotation = 180;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_RIGHT;
        } else if (positionTiledMap.y == 0) {
            velocity.y = currentVelocity;
            rotation = 90;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_DOWN;
        } else {
            velocity.y = -currentVelocity;
            rotation = 270;
        }

    }


    /**
     * Method that modifies the position of the enemy on the screen
     *
     * @param deltaTime frames per second
     */
    public void calculatePosition(float deltaTime) {

        // Modify the position of the enemy on the screen from its speed and DeltaTime
        positionScreen.x += deltaTime * velocity.x;
        positionScreen.y += deltaTime * velocity.y;
        count += Math.abs(currentVelocity) * deltaTime;

    }


    /**
     * Method for calculate the positionCell in the map if the enemy to route a cell
     */
    public void updatePositionCell() {

        // According to the direction of the enemy we modify the position vector map
        if (velocity.x < 0) {
            positionTiledMap.x -= 1;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_RIGHT;
        } else if (velocity.x > 0) {
            positionTiledMap.x += 1;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_LEFT;
        } else if (velocity.y < 0) {
            positionTiledMap.y -= 1;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_UP;
        } else if (velocity.y > 0) {
            positionTiledMap.y += 1;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_DOWN;
        }
    }


    /**
     * Method that checks if the enemy has moved a cell
     *
     * @return return true if is new cell or false ins't new cell
     */
    public boolean isNewCell() {


        if (velocity.x != 0) {//Moviment horizontal
            if (count >= Constants.SIZE_CELL_WIDTH) {
                count -= Constants.SIZE_CELL_WIDTH;
                return true;
            }
        } else if (velocity.y != 0) { //Moviment vertical
            if (count >= Constants.SIZE_CELL_HEIGHT) {
                count -= Constants.SIZE_CELL_HEIGHT;
                return true;
            }
        }

        return false;
    }

    /**
     * @return true if the last cell or false if isn't the last cell
     */
    public boolean isTheLastCell() {
        updatePositionCell();
        return MapHelper.instance.isExit((int) positionTiledMap.x, (int) positionTiledMap.y);
    }


    /**
     * @param damage The damage done to the enemy
     */
    public void calculateHealthAfterImpact(int damage) {

        setHealth(getHealth() - damage);

        // If life is less than or equal to zero indicate that the enemy
        // has been killed by a structure
        if (getHealth() <= 0) {
            isDeadForStructure = true;
        }
    }


    public void calculateDirectionIfEnemyIsConfused() {

        if (comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_UP)) {
            velocity.x = 0;
            velocity.y = currentVelocity;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_DOWN;
        } else if (comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_DOWN)) {
            velocity.x = 0;
            velocity.y = -currentVelocity;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_UP;
        } else if (comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_RIGHT)) {
            velocity.x = currentVelocity;
            velocity.y = 0;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_LEFT;
            flipX = false;
        } else if (comeFrom.equals(Constants.TILEDMAP_ATTRIBUTE_LEFT)) {
            velocity.x = -currentVelocity;
            velocity.y = 0;
            comeFrom = Constants.TILEDMAP_ATTRIBUTE_RIGHT;
            flipX = true;
        }
    }


    //==================================
    //        GETTERS & SETTERS
    //==================================


    public Constants.STATE_ENEMY getState() {
        return state;
    }

    public void setState(Constants.STATE_ENEMY state) {
        this.state = state;
    }

    public float getCountState() {
        return countState;
    }

    public void setCountState(float countState) {
        this.countState = countState;
    }

    /**
     * @return The direction of the enemy depending on the value of x and y shown in Vector2
     */
    public Vector2 getVelocity() {
        return velocity;
    }

    /**
     * @param velocity The direction of the enemy depending on the value of x and y shown in Vector2
     */
    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    /**
     * @return The moving speed of the enemy
     */
    public float getVelocityBase() {
        return velocityBase;
    }


    /**
     * @return The health of the enemy
     */
    public int getHealth() {
        return health;
    }

    /**
     * @param health The health of the enemy
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * @return The damage of the enemy
     */
    public int getDamage() {
        return damage;
    }

    /**
     * @param damage The damage of the enemy
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * @return The level of the enemy
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level The level of the enemy
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * @return A list with
     */
    public ArrayList<String> getAdvantages() {
        return advantages;
    }

    /**
     * @param advantages
     */
    public void setAdvantages(ArrayList<String> advantages) {
        this.advantages = advantages;
    }

    /**
     * @return
     */
    public ArrayList<String> getDisadvantages() {
        return disadvantages;
    }

    /**
     * @param disadvantages
     */
    public void setDisadvantages(ArrayList<String> disadvantages) {
        this.disadvantages = disadvantages;
    }

    /**
     * @return A counter that represents the position in pixels route on the map
     */
    public float getCount() {
        return count;
    }

    /**
     * @param count A counter that represents the position in pixels route on the map
     */
    public void setCount(float count) {
        this.count = count;
    }

    /**
     * @return The direction from which the enemy (left, right, up or down)
     */
    public String getComeFrom() {
        return comeFrom;
    }

    /**
     * @param comeFrom The direction from which the enemy (left, right, up or down)
     */
    public void setComeFrom(String comeFrom) {
        this.comeFrom = comeFrom;
    }

    /**
     * @return The imagesOfCanons representing the enemy
     */
    public AtlasRegion getImage() {
        return image;
    }

    /**
     * @param image The imagesOfCanons representing the enemy
     */
    public void setImage(AtlasRegion image) {
        this.image = image;
    }


    public boolean isDeadForStructure() {
        return isDeadForStructure;
    }

    public void setIsDeadForStructure(boolean isDeadForStructure) {
        this.isDeadForStructure = isDeadForStructure;
    }


    public Circle getAreaEnemy() {
        return areaEnemy;
    }

    public void setAreaEnemy(Circle areaEnemy) {
        this.areaEnemy = areaEnemy;
    }

    public float getCurrentVelocity() {
        return currentVelocity;
    }

    public void setCurrentVelocity(float currentVelocity) {
        this.currentVelocity = currentVelocity;
    }

    public boolean isConfused() {
        return isConfused;
    }

    public void setIsConfused(boolean isConfused) {
        this.isConfused = isConfused;
    }

    public boolean isInverted() {
        return inverted;
    }

    public void setInverted(boolean inverted) {
        this.inverted = inverted;
    }

    public float getCountStateConfused() {
        return countStateConfused;
    }

    public void setCountStateConfused(float countStateConfused) {
        this.countStateConfused = countStateConfused;
    }
}
