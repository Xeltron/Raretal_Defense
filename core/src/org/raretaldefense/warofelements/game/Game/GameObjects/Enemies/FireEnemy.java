package org.raretaldefense.warofelements.game.Game.GameObjects.Enemies;


import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;

public class FireEnemy extends AbstractEnemy {


    //==================================
    //          CONSTRUCTOR
    //==================================

    public FireEnemy(Vector2 positionTiledMap, Vector2 positionScren, Vector2 dimension,
                     Vector2 origin, Vector2 scale, float rotation) {
        super(positionTiledMap, positionScren, dimension, origin, scale, rotation, Constants.VELOCITY_BASE_FIRE_ENEMY,
                Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_FIRE_ENEMY));

        // We define the properties of each enemy

        // Add the advantages of the enemy
        this.getAdvantages().add(Constants.WIND_ELEMENT);
        this.getAdvantages().add(Constants.FIRE_ELEMENT);

        // Add the disadvantages of the enemy
        this.getDisadvantages().add(Constants.WATER_ELEMENT);
        this.getDisadvantages().add(Constants.ROCK_TRAP);

        // Add the properties of the enemy
        this.health = Constants.HEALTH_FIRE_ENEMY;
        this.damage = Constants.DAMAGE_FIRE_ENEMY;
        this.velocity = new Vector2();
        this.positionScreen = positionScren;


        this.areaEnemy = new Circle(image.offsetX, image.offsetY, image.getRegionWidth());

        init();
    }


}
