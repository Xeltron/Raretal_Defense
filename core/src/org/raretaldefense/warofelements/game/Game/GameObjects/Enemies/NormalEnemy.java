package org.raretaldefense.warofelements.game.Game.GameObjects.Enemies;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;

public class NormalEnemy extends AbstractEnemy {

    //==================================
    //          CONSTRUCTOR
    //==================================

    public NormalEnemy(Vector2 positionTiledMap, Vector2 positionScren, Vector2 dimension,
                       Vector2 origin, Vector2 scale, float rotation) {
        super(positionTiledMap, positionScren, dimension, origin, scale, rotation, Constants.VELOCITY_BASE_NORMAL_ENEMY,
                Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_NORMAL_ENEMY));

        // We define the properties of each enemy

        // Add the advantages of the enemy
        this.getAdvantages().add(Constants.THUNDER_ELEMENT);
        this.getAdvantages().add(Constants.ROCK_TRAP);

        // Add the disadvantages of the enemy
        this.getDisadvantages().add(Constants.FIRE_ELEMENT);
        this.getDisadvantages().add(Constants.WIND_ELEMENT);

        // Add the properties of the enemy
        this.health = Constants.HEALTH_NORMAL_ENEMY;
        this.damage = Constants.DAMAGE_NORMAL_ENEMY;
        this.velocity = new Vector2();
        this.positionScreen = positionScren;

        this.areaEnemy = new Circle(image.offsetX, image.offsetY, image.getRegionWidth());


        init();

    }
}
