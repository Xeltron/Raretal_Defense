package org.raretaldefense.warofelements.game.Game.GameObjects.Enemies;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;


public class WaterEnemy extends AbstractEnemy {

    //==================================
    //          CONSTRUCTOR
    //==================================

    public WaterEnemy(Vector2 positionTiledMap, Vector2 positionScren, Vector2 dimension,
                      Vector2 origin, Vector2 scale, float rotation) {
        super(positionTiledMap, positionScren, dimension, origin, scale, rotation, Constants.VELOCITY_BASE_WATER_ENEMY,
                Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_WATER_ENEMY));

        // We define the properties of each enemy

        // Add the advantages of the enemy
        this.getAdvantages().add(Constants.FIRE_ELEMENT);
        this.getAdvantages().add(Constants.WATER_ELEMENT);

        // Add the disadvantages of the enemy
        this.getDisadvantages().add(Constants.THUNDER_ELEMENT);
        this.getDisadvantages().add(Constants.WIND_ELEMENT);

        // Add the properties of the enemy
        this.setHealth(Constants.HEALTH_WATER_ENEMY);
        this.setDamage(Constants.DAMAGE_WATER_ENEMY);
        this.setVelocity(new Vector2());
        this.setPositionScreen(positionScren);

        this.areaEnemy = new Circle(image.offsetX, image.offsetY, image.getRegionWidth());


        init();
    }
}
