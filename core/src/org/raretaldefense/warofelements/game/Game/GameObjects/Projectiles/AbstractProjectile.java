package org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Game.GameObjects.AbstractGameObject;
import org.raretaldefense.warofelements.game.Game.GameObjects.Enemies.AbstractEnemy;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.AbstractTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.AbstractTrap;
import org.raretaldefense.warofelements.game.Helpers.Assets;
import org.raretaldefense.warofelements.game.Helpers.MapHelper;

public class AbstractProjectile extends AbstractGameObject {


    //==================================
    //           ATTRIBUTES
    //==================================

    protected final String TAG = getClass().getSimpleName();
    protected int damage;
    protected float velocityBase;
    protected Vector2 positionProjectile;
    protected Vector2 positionTarget;
    protected AbstractEnemy enemy;
    protected TextureAtlas.AtlasRegion image;
    protected AbstractTower tower;
    protected float distanceToTheTarget = 0;
    protected AbstractTrap rockTrap;
    protected Vector2 positionRockTrap;
    protected Ellipse areaProjectile;


    //==================================
    //           CONSTRUCTOR
    //==================================


    /**
     * @param tower The tower from which the projectile is fired
     * @param enemy The enemy where it has to impact the projectile
     * @param image The image of the projectile
     */
    public AbstractProjectile(AbstractTower tower, AbstractEnemy enemy, TextureAtlas.AtlasRegion image) {
        super(null, null, null, null, new Vector2(1, 1), 0);

        // Initialize the basic builder
        if (tower != null && enemy != null) {
            this.enemy = enemy;
            this.damage = tower.getCurrentDamage();
            this.positionScreen = new Vector2(tower.getPositionScreen().x, tower.getPositionScreen().y);
            this.positionProjectile = new Vector2(tower.getPositionScreen().x + tower.getDimension().x / 2,
                    tower.getPositionScreen().y + tower.getDimension().y / 2);
            this.positionTarget = new Vector2(enemy.getPositionScreen().x, enemy.getPositionScreen().y);
            this.tower = tower;
        }

        this.image = image;

        this.dimension = Assets.instance.dimensionsOfGameObject(image.getRegionWidth(), image.getRegionHeight());

    }


    //==================================
    //         OVERRIDE METHODS
    //==================================

    @Override
    public void update(float deltaTime) {
        // Calculate position of projectile in the screen
        calculatePositionProjectile(deltaTime);
    }

    @Override
    public void render(SpriteBatch batch) {

        // Painted the projectile
        batch.draw(image.getTexture(), positionProjectile.x, positionProjectile.y, dimension.x / 2, dimension.y / 2,
                dimension.x, dimension.y, 1, 1, rotation - 90,
                image.getRegionX(), image.getRegionY(), image.getRegionWidth(), image.getRegionHeight(),
                false, false);


    }


    //==================================
    //            METHODS
    //==================================


    /**
     * Method for calculate position of projectile in the screen
     *
     * @param deltaTime frames for second
     */
    public void calculatePositionProjectile(float deltaTime) {

        // If the enemy is killed by another projectile this projectile
        // will go to the last position of the enemy
        if (enemy != null) {
            // Get the enemy's position
            positionTarget.x = enemy.getPositionScreen().x;
            positionTarget.y = enemy.getPositionScreen().y;
        }


        // Get a vector from the position of the projectile and the enemy
        Vector2 vectorBetweenPoints = new Vector2(positionTarget.x - positionProjectile.x, positionTarget.y - positionProjectile.y);


        // Calculate the distance to the target
        distanceToTheTarget = (float) Math.sqrt(Math.pow(vectorBetweenPoints.x, 2) + Math.pow(vectorBetweenPoints.y, 2));

        // Get the angle of rotation between the projectile and the enemy
        float angleOfRotation;

        if (vectorBetweenPoints.x != 0) {
            angleOfRotation = (float) Math.atan(vectorBetweenPoints.y / vectorBetweenPoints.x);
        } else {
            if (vectorBetweenPoints.y > 0) {
                angleOfRotation = 90;
            } else {
                angleOfRotation = 270;
            }
        }

        // Radians to convert the angles
        angleOfRotation = angleOfRotation * 360 / (2 * (float) Math.PI);


        if (vectorBetweenPoints.y < 0) {
            angleOfRotation += 180;
        }


        // Set the rotation of projectile
        setRotation(angleOfRotation);


        float cosAngle = vectorBetweenPoints.x / distanceToTheTarget;
        float sinAngle = vectorBetweenPoints.y / distanceToTheTarget;

        // Change the position of the projectile on the screen
        positionProjectile.x += deltaTime * getVelocityBase() * cosAngle;
        positionProjectile.y += deltaTime * getVelocityBase() * sinAngle;


    }

    /**
     * @return True if the projectile has hit the enemy or false if it has not impacted
     */
    public boolean isImpacted() {

        // Look if the area of the projectile is in the position of the enemy
        if (distanceToTheTarget < MapHelper.instance.getWidthCellInPixels() * .1) {
            setDead(true);
            return true;
        }
        return false;
    }


    //==================================
    //        GETTERS & SETTERS
    //==================================


    /**
     * @return Projectile damage, this damage varies according to the tower from where fires
     */
    public int getDamage() {
        return damage;
    }

    /**
     * @param damage Projectile damage, this damage varies according to the tower from where fires
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * @return The moving speed of the projectile
     */
    public float getVelocityBase() {
        return velocityBase;
    }

    /**
     * @param velocityBase The moving speed of the projectile
     */
    public void setVelocityBase(int velocityBase) {
        this.velocityBase = velocityBase;
    }

    /**
     * @return The position of projectile on the screen shown in Vector2
     */
    public Vector2 getPositionProjectile() {
        return positionProjectile;
    }

    /**
     * @param positionProjectile The position of projectile on the screen shown in Vector2
     */
    public void setPositionProjectile(Vector2 positionProjectile) {
        this.positionProjectile = positionProjectile;
    }

    /**
     * @return The position of enemy on the screen shown in Vector2
     */
    public Vector2 getPositionTarget() {
        return positionTarget;
    }

    /**
     * @param positionTarget The position of enemy on the screen shown in Vector2
     */
    public void setPositionTarget(Vector2 positionTarget) {
        this.positionTarget = positionTarget;
    }

    /**
     * @return The enemy where it has to impact the projectile
     */
    public AbstractEnemy getEnemy() {
        return enemy;
    }

    /**
     * @param enemy The enemy where it has to impact the projectile
     */
    public void setEnemy(AbstractEnemy enemy) {
        this.enemy = enemy;
    }


    /**
     * @return The imagesOfCanons representing the projectile
     */
    public TextureAtlas.AtlasRegion getImage() {
        return image;
    }

    /**
     * @param image The imagesOfCanons representing the projectile
     */
    public void setImage(TextureAtlas.AtlasRegion image) {
        this.image = image;
    }

    /**
     * @return The tower from which the projectile is fired
     */
    public AbstractTower getTower() {
        return tower;
    }

    /**
     * @param tower The tower from which the projectile is fired
     */
    public void setTower(AbstractTower tower) {
        this.tower = tower;
    }

    public void setVelocityBase(float velocityBase) {
        this.velocityBase = velocityBase;
    }

    public float getDistanceToTheTarget() {
        return distanceToTheTarget;
    }

    public void setDistanceToTheTarget(float distanceToTheTarget) {
        this.distanceToTheTarget = distanceToTheTarget;
    }

    public AbstractTrap getRockTrap() {
        return rockTrap;
    }

    public void setRockTrap(AbstractTrap rockTrap) {
        this.rockTrap = rockTrap;
    }

    public Vector2 getPositionRockTrap() {
        return positionRockTrap;
    }

    public void setPositionRockTrap(Vector2 positionRockTrap) {
        this.positionRockTrap = positionRockTrap;
    }

    public Ellipse getAreaProjectile() {
        return areaProjectile;
    }

    public void setAreaProjectile(Ellipse areaProjectile) {
        this.areaProjectile = areaProjectile;
    }
}
