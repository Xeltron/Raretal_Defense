package org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.AbstractTrap;
import org.raretaldefense.warofelements.game.Helpers.Assets;


public class RockProjectile extends AbstractProjectile {


    //==================================
    //           CONSTRUCTOR
    //==================================

    public RockProjectile(Vector2 positionScreenBase, AbstractTrap rockTrap) {
        super(null, null, Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_ROCK_PROJECTILE));


        // Add the properties of the projectile
        velocityBase = Constants.VELOCITY_BASE_ROCK_PROJECTILE;
        this.positionScreen = positionScreenBase;

        this.rockTrap = rockTrap;
        this.damage = rockTrap.getDamage();
        this.positionRockTrap = new Vector2(rockTrap.getPositionScreen().x, rockTrap.getPositionScreen().y);


    }


    //==================================
    //         OVERRIDE METHODS
    //==================================

    @Override
    public void update(float deltaTime) {
        // Calculate position of projectile in the screen
        calculatePositionProjectile(deltaTime);
    }

    @Override
    public void render(SpriteBatch batch) {

        // Painted the projectile
        batch.draw(image.getTexture(), positionScreen.x, positionScreen.y, dimension.x / 5, dimension.y / 3,
                dimension.x, dimension.y, 1.5f, 1.5f, rotation - 90,
                image.getRegionX(), image.getRegionY(), image.getRegionWidth(), image.getRegionHeight(),
                false, false);


    }


    //==================================
    //            METHODS
    //==================================


    /**
     * Method for calculate position of projectile in the screen
     *
     * @param deltaTime frames for second
     */
    public void calculatePositionProjectile(float deltaTime) {

        // If the enemy is killed by another projectile this projectile
        // will go to the last position of the enemy
        if (rockTrap != null) {
            // Get the enemy's position
            positionRockTrap.x = rockTrap.getPositionScreen().x + rockTrap.getImage().getRegionWidth() / 5f;
            positionRockTrap.y = rockTrap.getPositionScreen().y + rockTrap.getImage().getRegionHeight() / 5f;
        }


        // Get a vector from the position of the projectile and the enemy
        Vector2 vectorBetweenPoints = new Vector2(positionRockTrap.x - positionScreen.x, positionRockTrap.y - positionScreen.y);


        // Calculate the distance to the target
        distanceToTheTarget = (float) Math.sqrt(Math.pow(vectorBetweenPoints.x, 2) + Math.pow(vectorBetweenPoints.y, 2));

        // Get the angle of rotation between the projectile and the enemy
        float angleOfRotation;

        if (vectorBetweenPoints.x != 0) {
            angleOfRotation = (float) Math.atan(vectorBetweenPoints.y / vectorBetweenPoints.x);
        } else {
            if (vectorBetweenPoints.y > 0) {
                angleOfRotation = 90;
            } else {
                angleOfRotation = 270;
            }
        }

        // Radians to convert the angles
        angleOfRotation = angleOfRotation * 360 / (2 * (float) Math.PI);


        if (vectorBetweenPoints.y < 0) {
            angleOfRotation += 180;
        }


        // Set the rotation of projectile
        setRotation(angleOfRotation);


        float cosAngle = vectorBetweenPoints.x / distanceToTheTarget;
        float sinAngle = vectorBetweenPoints.y / distanceToTheTarget;

        // Change the position of the projectile on the screen
        positionScreen.x += deltaTime * getVelocityBase() * cosAngle;
        positionScreen.y += deltaTime * getVelocityBase() * sinAngle;


    }

}
