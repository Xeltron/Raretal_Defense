package org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Game.GameObjects.Enemies.AbstractEnemy;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.AbstractTower;
import org.raretaldefense.warofelements.game.Helpers.Assets;


public class WindProjectile extends AbstractProjectile {

    //==================================
    //           CONSTRUCTOR
    //==================================

    public WindProjectile(AbstractTower tower, AbstractEnemy enemy) {
        super(tower, enemy, Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_WIND_PROJECTILE));


        // Add the properties of the projectile
        velocityBase = Constants.VELOCITY_BASE_WIND_PROJECTILE;


    }
}
