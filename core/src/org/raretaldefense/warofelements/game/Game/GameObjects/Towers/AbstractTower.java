package org.raretaldefense.warofelements.game.Game.GameObjects.Towers;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import org.raretaldefense.warofelements.game.Game.GameObjects.AbstractGameObject;
import org.raretaldefense.warofelements.game.Game.GameObjects.Enemies.AbstractEnemy;
import org.raretaldefense.warofelements.game.Helpers.Assets;
import org.raretaldefense.warofelements.game.Helpers.MapHelper;

import java.util.List;

public abstract class AbstractTower extends AbstractGameObject {


    //==================================
    //           ATTRIBUTES
    //==================================

    private final String TAG = AbstractTower.class.getSimpleName();

    protected int level;
    protected float[] velocityAttack;
    protected int[] damage;
    protected int[] value;
    protected int[] range;
    protected TextureAtlas.AtlasRegion imageBase;
    protected Array<TextureAtlas.AtlasRegion> imagesOfCanons;
    protected AbstractEnemy target;
    protected Vector2 dimensionBase;
    protected float count = 0;
    protected boolean canShoot = false;
    protected Ellipse area;


    //==================================
    //           CONSTRUCTOR
    //==================================


    /**
     * @param positionTiledMap The cell position in the map (TiledMap) shown in Vector2
     * @param positionScreen   The pixel position on the screen shown in Vector2
     * @param dimension        The dimension of the object shown in Vector2
     * @param origin           The origin of the object shown in Vector2
     * @param scale            The scale of the object shown in Vector2
     * @param rotation         The rotation of the object
     * @param velocityAttack   The velocity attack of the tower
     */
    public AbstractTower(Vector2 positionTiledMap, Vector2 positionScreen, Vector2 dimension,
                         Vector2 origin, Vector2 scale, float rotation, float[] velocityAttack,
                         Array<TextureAtlas.AtlasRegion> imagesOfCanons, TextureAtlas.AtlasRegion imageBase) {
        super(positionTiledMap, positionScreen, dimension, origin, scale, rotation);

        // Initialize the basic builder
        this.level = 0; // The level of the tower
        this.velocityAttack = velocityAttack;

        // Assigned the imagesOfCanons for base
        this.imageBase = imageBase;
        // Assigned the imagesOfCanons for canon
        this.imagesOfCanons = imagesOfCanons;
        // We asigned the correct dimensions for the images of tower
        this.dimension = Assets.instance.dimensionsOfGameObject(imagesOfCanons.get(level).getRegionWidth(), imagesOfCanons.get(level).getRegionHeight());
        this.dimensionBase = Assets.instance.dimensionsOfGameObject(imageBase.getRegionWidth(), imageBase.getRegionHeight());

        this.area = new Ellipse(0, 0, 0, 0);


    }


    //==================================
    //        OVERRIDE METHODS
    //==================================

    @Override
    public void update(float deltaTime) {

        // Check if your enemy is still visible in the area of the tower
        if (isTargetVisible()) {
            // Rotate the cannon and check if you can shoot the tower
            calculateRotationCannon();
            checkShootingTime(deltaTime);
        }
    }

    @Override
    public void render(SpriteBatch batch) {


        // Painted the base of tower
        batch.draw(imageBase.getTexture(), positionScreen.x, positionScreen.y, imageBase.offsetX, imageBase.offsetY,
                dimensionBase.x, dimensionBase.y, 1, 1, 0,
                imageBase.getRegionX(), imageBase.getRegionY(), imageBase.getRegionWidth(), imageBase.getRegionHeight(),
                false, false);


        // Painted the cannon
        batch.draw(imagesOfCanons.get(level).getTexture(),
                positionScreen.x + dimension.x / 4, positionScreen.y + dimension.y / 2,
                dimension.x / 2, dimension.y / 2 - 5,
                dimension.x, dimension.y, 1.25f, 1.25f, rotation,
                imagesOfCanons.get(level).getRegionX(), imagesOfCanons.get(level).getRegionY(),
                imagesOfCanons.get(level).getRegionWidth(), imagesOfCanons.get(level).getRegionHeight(),
                false, false);
    }


    //==================================
    //             METHODS
    //==================================


    /**
     * Method to see if the enemy is in the area of the tower
     * if the enemy is not visible, we change the target of the tower to null
     *
     * @return True if the enemy is still visible in the area of the tower or false if it is not visible
     */
    public boolean isTargetVisible() {

        if (target != null) {
            // Look the enemy is in the area of the tower from the enemy's position on the screen
            if (area.contains(target.getPositionScreen().x, target.getPositionScreen().y)) {
                return true;
            } else {
                // Remove your target if it is not visible in your area to assign another
                setTarget(null);
            }
        }
        return false;
    }


    /**
     * Method that checks if the tower can shoot,
     *
     * @param deltatime frames per second
     */
    public void checkShootingTime(float deltatime) {

        // Increment the counter from the time elapsed
        count += deltatime;

        if (count >= getCurrentVelocityAtack()) {
            count -= getCurrentVelocityAtack();
            // Indicate that you can shoot
            canShoot = true;
        } else {
            canShoot = false;
        }

    }


    /**
     * Method to calculate the rotation of the cannon of the tower about the enemy
     */
    public void calculateRotationCannon() {

        // Get the position of the tower
        float pxTower = positionScreen.x;
        float pyTower = positionScreen.y;
        // Get the enemy's position
        float pxEnemy = target.getPositionScreen().x;
        float pyEnemy = target.getPositionScreen().y;


        // Get a vector from the position of the tower and the enemy
        Vector2 vectorBetweenPoints = new Vector2(pxEnemy - pxTower, pyEnemy - pyTower);

        // Get the angle of rotation between the tower and the enemy
        double angleOfRotation = Math.atan(vectorBetweenPoints.y / vectorBetweenPoints.x);

        // Radians to convert the angles
        angleOfRotation = angleOfRotation * 360 / (2 * Math.PI) - 90;

        if (vectorBetweenPoints.x > 0) {
            if (vectorBetweenPoints.y > 0) {

            } else if (vectorBetweenPoints.y < 0) {
                angleOfRotation -= 360;
            }
        } else if (vectorBetweenPoints.y > 0) {
            angleOfRotation -= 180;
        } else if (vectorBetweenPoints.y < 0) {
            angleOfRotation += 180;
        }

        // Set the rotation of tower
        setRotation((float) angleOfRotation);

    }


    /**
     * Method that assigns a target enemy as if in the area of the tower
     *
     * @param listOfEnemies List of living enemies on the map
     */
    public void asignEnemy(List<AbstractEnemy> listOfEnemies) {

        for (AbstractEnemy currentEnemy : listOfEnemies) {
            // Check that the enemy is in the area
            if (area.contains(currentEnemy.getPositionScreen())) {
                // Assigned the enemy
                setTarget(currentEnemy);
                return;
            }
        }

    }

    public void evolveTower() {

        if (level < imagesOfCanons.size) {
            level++;

            if (range != null && range.length > level) {
                area.setSize(MapHelper.instance.getWidthCellInPixels() * (range[level] + 2), MapHelper.instance.getHeightCellInPixels() * (range[level] + 2));
            }


        }

    }


    public boolean canEvolve() {
        return level < imagesOfCanons.size - 1;
    }


    //==================================
    //        GETTERS & SETTERS
    //==================================


    public int getCurrentDamage() {

        if (level >= damage.length) {
            return damage[damage.length - 1];
        }

        return damage[level];
    }

    public float getCurrentRange() {
        if (level >= range.length) {
            return range[range.length - 1];
        }

        return range[level];
    }

    public int getCurrentValue() {

        if (level >= value.length) {
            return value[value.length - 1];
        }

        return value[level];
    }

    public int getNextValue() {
        if (level + 1 >= value.length) {
            return value[value.length - 1];
        }

        return value[level + 1];
    }

    public float getCurrentVelocityAtack() {


        if (level >= velocityAttack.length) {
            return velocityAttack[velocityAttack.length - 1];
        }

        return velocityAttack[level];

    }

    public void setArea(Vector2 position) {

        if (level < range.length) {
            this.area.setPosition(position.x + dimensionBase.x / 2, position.y + dimensionBase.y / 2);
            this.area.setSize(MapHelper.instance.getWidthCellInPixels() * (range[level] + 2), MapHelper.instance.getHeightCellInPixels() * (range[level] + 2));
        }
    }


    public int[] getDamage() {
        return damage;
    }

    public void setDamage(int[] damage) {
        this.damage = damage;
    }

    public int[] getValue() {
        return value;
    }

    public void setValue(int[] value) {
        this.value = value;
    }

    public int[] getRange() {
        return range;
    }

    public void setRange(int[] range) {
        this.range = range;
    }

    /**
     * @return The level of the tower (0, 1 or 2)
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level The level of the tower (0, 1 or 2)
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * @return The velocity attack of the tower
     */
    public float[] getVelocityAttack() {
        return velocityAttack;
    }

    /**
     * @param velocityAttack The velocity attack of the tower
     */
    public void setVelocityAttack(float[] velocityAttack) {
        this.velocityAttack = velocityAttack;
    }


    /**
     * @return Within range of the tower shown in a Circle
     */
    public Ellipse getArea() {
        return area;
    }

    /**
     * @param area Within range of the tower shown in a Circle
     */
    public void setArea(Ellipse area) {
        this.area = area;
    }

    /**
     * @return Image that represents the base of the tower
     */
    public TextureAtlas.AtlasRegion getImageBase() {
        return imageBase;
    }

    /**
     * @param imageBase Image that represents the base of the tower
     */
    public void setImageBase(TextureAtlas.AtlasRegion imageBase) {
        this.imageBase = imageBase;
    }

    /**
     * @return Image that represents the tower
     */
    public Array<TextureAtlas.AtlasRegion> getImagesOfCanons() {
        return imagesOfCanons;
    }

    /**
     * @param imagesOfCanons Image that represents the tower
     */
    public void setImagesOfCanons(Array<TextureAtlas.AtlasRegion> imagesOfCanons) {
        this.imagesOfCanons = imagesOfCanons;
    }

    /**
     * @return Enemy attacks continue and the tower
     */
    public AbstractEnemy getTarget() {
        return target;
    }

    /**
     * @param target Enemy attacks continue and the tower
     */
    public void setTarget(AbstractEnemy target) {
        this.target = target;
    }

    /**
     * @return Counter compared with the speed of attack of the tower, if it is equal or higher can shoot
     */
    public float getCount() {
        return count;
    }

    /**
     * @param count Counter compared with the speed of attack of the tower, if it is equal or higher can shoot
     */
    public void setCount(float count) {
        this.count = count;
    }

    /**
     * @return True if can shot the tower or false if can't shot
     */
    public boolean isCanShoot() {
        return canShoot;
    }

    /**
     * @param canShoot True if can shot the tower or false if can't shot
     */
    public void setCanShoot(boolean canShoot) {
        this.canShoot = canShoot;
    }


}
