package org.raretaldefense.warofelements.game.Game.GameObjects.Towers;

import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;

/**
 * Created by iam47189294 on 20/04/16.
 */
public class ThunderTower extends AbstractTower {

    //==================================
    //          CONSTRUCTOR
    //==================================

    public ThunderTower(Vector2 positionTiledMap, Vector2 positionScreen, Vector2 dimension,
                        Vector2 origin, Vector2 scale, float rotation) {
        super(positionTiledMap, positionScreen, dimension, origin, scale, rotation, Constants.VELOCITY_ATTACK_THUNDER_TOWER,
                Assets.instance.getTextureAtlasGame().findRegions(Constants.REFERENCE_NAME_ATLAS_THUNDER_TOWER),
                Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BASE_THUNDER_TOWER));

        // We define the properties of each enemy

        // Add the properties of the enemy
        this.damage = Constants.DAMAGE_THUNDER_TOWER;
        this.range = Constants.AREA_ATTACK_THUNDER_TOWER;
        this.value = Constants.VALUE_THUNDER_TOWER;
        this.positionScreen = positionScreen;

    }


}
