package org.raretaldefense.warofelements.game.Game.GameObjects.Towers;

import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;


public class WaterTower extends AbstractTower {

    //==================================
    //          CONSTRUCTOR
    //==================================

    public WaterTower(Vector2 positionTiledMap, Vector2 positionScreen, Vector2 dimension,
                      Vector2 origin, Vector2 scale, float rotation) {
        super(positionTiledMap, positionScreen, dimension,
                origin, scale, rotation, Constants.VELOCITY_ATTACK_WATER_TOWER,
                Assets.instance.getTextureAtlasGame().findRegions(Constants.REFERENCE_NAME_ATLAS_WATER_TOWER),
                Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BASE_WATER_TOWER));

        // We define the properties of each enemy

        // Add the properties of the enemy
        this.range = Constants.AREA_ATTACK_WATER_TOWER;
        this.damage = Constants.DAMAGE_WATER_TOWER;
        this.value = Constants.VALUE_WATER_TOWER;
        this.positionScreen = positionScreen;

    }
}
