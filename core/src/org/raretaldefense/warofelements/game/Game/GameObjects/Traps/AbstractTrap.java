package org.raretaldefense.warofelements.game.Game.GameObjects.Traps;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Game.GameObjects.AbstractGameObject;
import org.raretaldefense.warofelements.game.Helpers.Assets;

public abstract class AbstractTrap extends AbstractGameObject {


    //==================================
    //           ATTRIBUTES
    //==================================

    private final String TAG = AbstractTrap.class.getSimpleName();

    protected int level, countImage = 1;
    protected float[] cooldown, cooldownActiveTrap;
    protected int[] damage;
    protected int[] value;
    protected TextureAtlas.AtlasRegion image;
    protected Array<TextureAtlas.AtlasRegion> imagesOfClocks;
    protected Vector2 dimensionClock;
    protected float count = 0;
    protected float countFirstQuarterTime, countSecondQuarterTime, countThirdQuarterTime, countFourtQuarterTime;
    protected boolean itCanBeUsed = true;
    protected boolean isActive = false;
    protected boolean theActivationPast = false;
    protected int shoots = 0;


    //==================================
    //           CONSTRUCTOR
    //==================================

    /**
     * @param positionTiledMap The cell position in the map (TiledMap) shown in Vector2
     * @param positionScreen   The pixel position on the screen shown in Vector2
     * @param dimension        The dimension of the object shown in Vector2
     * @param origin           The origin of the object shown in Vector2
     * @param scale            The scale of the object shown in Vector2
     * @param rotation         The rotation of the object
     * @param image            The image of the trap
     * @param imagesOfClocks   The images of the clock, represented the cooldown of the trap
     */
    public AbstractTrap(Vector2 positionTiledMap, Vector2 positionScreen, Vector2 dimension,
                        Vector2 origin, Vector2 scale, float rotation, TextureAtlas.AtlasRegion image,
                        Array<TextureAtlas.AtlasRegion> imagesOfClocks) {
        super(positionTiledMap, positionScreen, dimension, origin, scale, rotation);

        // Initialize the basic builder
        this.level = 0; // The level of the trap

        this.image = image;
        this.imagesOfClocks = imagesOfClocks;

        // We asigned the correct dimensions for the images of tower
        this.dimension = Assets.instance.dimensionsOfGameObject(image.getRegionWidth(), image.getRegionHeight());
        this.dimensionClock = Assets.instance.dimensionsOfGameObject(imagesOfClocks.get(1).getRegionWidth(), imagesOfClocks.get(1).getRegionHeight());

    }


    //==================================
    //         OVERRIDE METHODS
    //==================================

    @Override
    public void update(float deltaTime) {

        if (!itCanBeUsed) {
            shoots += 1;

            if (theActivationPast) {
                checkTimeCooldown(deltaTime);
            } else {
                checkActiveTimeTrap(deltaTime);
            }
        }
    }

    @Override
    public void render(SpriteBatch batch) {


        // Painted the base of tower
        batch.draw(image.getTexture(), positionScreen.x, positionScreen.y, 0, 0,
                dimension.x, dimension.y, 1, 1, rotation,
                image.getRegionX(), image.getRegionY(), image.getRegionWidth(), image.getRegionHeight(),
                false, false);


        // Painted the cannon
        if (theActivationPast) {
            batch.draw(imagesOfClocks.get(countImage).getTexture(),
                    positionScreen.x + dimensionClock.x * 1.2f, positionScreen.y - dimensionClock.y / 6,
                    dimensionClock.x / 2, dimensionClock.y / 2 - 5,
                    dimensionClock.x, dimensionClock.y, 1.25f, 1.25f, 0,
                    imagesOfClocks.get(countImage).getRegionX(), imagesOfClocks.get(countImage).getRegionY(),
                    imagesOfClocks.get(countImage).getRegionWidth(), imagesOfClocks.get(countImage).getRegionHeight(),
                    false, false);
        }


    }


    //==================================
    //            METHODS
    //==================================


    public void checkTimeCooldown(float deltaTime) {

        count += deltaTime;
        pictureAssignTime(count);

        if (count >= cooldown[level]) {
            count -= cooldown[level];
            countImage = 0;
            shoots = 0;
            count = 0;
            itCanBeUsed = true;
        }
    }


    public void pictureAssignTime(float count) {

        calculateTimeClock();

        if (count > countFirstQuarterTime && count < countSecondQuarterTime) {
            countImage = 2;
        } else if (count > countSecondQuarterTime && count < countThirdQuarterTime) {
            countImage = 3;
        } else if (count > countThirdQuarterTime && count < countFourtQuarterTime) {
            countImage = 4;
        } else if (count > countThirdQuarterTime && count < cooldown[level]) {
            countImage = 5;
        }

    }

    public void checkActiveTimeTrap(float deltaTime) {

        count += deltaTime;
        if (count < cooldownActiveTrap[level]) {
            isActive = true;
            theActivationPast = false;

            if (this instanceof Trapdoor) {
                this.setImage(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_TRAPDOOR_OPEN));
            } else if (this instanceof ConfusionTrap) {
                this.setImage(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_CONFUSION_TRAP_ACTIVATE));
            }
        } else {
            isActive = false;
            theActivationPast = true;
            count = 0;
            countImage = 1;

            if (this instanceof Trapdoor) {
                this.setImage(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_TRAPDOOR_CLOSE));
            } else if (this instanceof ConfusionTrap) {
                this.setImage(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_CONFUSION_TRAP));
            }
        }
    }


    public void calculateTimeClock() {

        this.countFirstQuarterTime = 25 * cooldown[level] / 100;
        this.countSecondQuarterTime = 50 * cooldown[level] / 100;
        this.countThirdQuarterTime = 75 * cooldown[level] / 100;
        this.countFourtQuarterTime = 95 * cooldown[level] / 100;
    }


    //==================================
    //        GETTERS & SETTERS
    //==================================


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCountImage() {
        return countImage;
    }

    public void setCountImage(int countImage) {
        this.countImage = countImage;
    }

    public float getCooldown() {
        return cooldown[level];
    }

    public void setCooldown(float[] cooldown) {
        this.cooldown = cooldown;
    }

    public float getCooldownActiveTrap() {
        return cooldownActiveTrap[level];
    }

    public void setCooldownActiveTrap(float[] cooldownActiveTrap) {
        this.cooldownActiveTrap = cooldownActiveTrap;
    }

    public int getDamage() {

        return damage[level];
    }

    public void setDamage(int[] damage) {
        this.damage = damage;
    }

    public int getValue() {

        if (level >= value.length) {
            return value[value.length - 1];
        }

        return value[level];
    }

    public void setValue(int[] value) {
        this.value = value;
    }

    public TextureAtlas.AtlasRegion getImage() {
        return image;
    }

    public void setImage(TextureAtlas.AtlasRegion image) {
        this.image = image;
    }

    public Array<TextureAtlas.AtlasRegion> getImagesOfClocks() {
        return imagesOfClocks;
    }

    public void setImagesOfClocks(Array<TextureAtlas.AtlasRegion> imagesOfClocks) {
        this.imagesOfClocks = imagesOfClocks;
    }

    public Vector2 getDimensionClock() {
        return dimensionClock;
    }

    public void setDimensionClock(Vector2 dimensionClock) {
        this.dimensionClock = dimensionClock;
    }

    public float getCount() {
        return count;
    }

    public void setCount(float count) {
        this.count = count;
    }

    public float getCountFirstQuarterTime() {
        return countFirstQuarterTime;
    }

    public void setCountFirstQuarterTime(float countFirstQuarterTime) {
        this.countFirstQuarterTime = countFirstQuarterTime;
    }

    public float getCountSecondQuarterTime() {
        return countSecondQuarterTime;
    }

    public void setCountSecondQuarterTime(float countSecondQuarterTime) {
        this.countSecondQuarterTime = countSecondQuarterTime;
    }

    public float getCountThirdQuarterTime() {
        return countThirdQuarterTime;
    }

    public void setCountThirdQuarterTime(float countThirdQuarterTime) {
        this.countThirdQuarterTime = countThirdQuarterTime;
    }

    public float getCountFourtQuarterTime() {
        return countFourtQuarterTime;
    }

    public void setCountFourtQuarterTime(float countFourtQuarterTime) {
        this.countFourtQuarterTime = countFourtQuarterTime;
    }

    public boolean isItCanBeUsed() {
        return itCanBeUsed;
    }

    public void setItCanBeUsed(boolean itCanBeUsed) {
        this.itCanBeUsed = itCanBeUsed;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public int getShoots() {
        return shoots;
    }

    public void setShoots(int shoots) {
        this.shoots = shoots;
    }

    public boolean isTheActivationPast() {
        return theActivationPast;
    }

    public void setTheActivationPast(boolean theActivationPast) {
        this.theActivationPast = theActivationPast;
    }
}
