package org.raretaldefense.warofelements.game.Game.GameObjects.Traps;

import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;


public class RockTrap extends AbstractTrap {


    //==================================
    //           CONSTRUCTOR
    //==================================


    /**
     * @param positionTiledMap The cell position in the map (TiledMap) shown in Vector2
     * @param positionScreen   The pixel position on the screen shown in Vector2
     * @param dimension        The dimension of the object shown in Vector2
     * @param origin           The origin of the object shown in Vector2
     * @param scale            The scale of the object shown in Vector2
     * @param rotation         The rotation of the object
     */
    public RockTrap(Vector2 positionTiledMap, Vector2 positionScreen, Vector2 dimension,
                    Vector2 origin, Vector2 scale, float rotation) {
        super(positionTiledMap, positionScreen, dimension, origin, scale, rotation,
                Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_ROCK_TRAP),
                Assets.instance.getTextureAtlasGame().findRegions(Constants.REFERENCE_NAME_ATLAS_COOLDOWN_TRAPS));

        // Initialize the basic builder
        this.damage = Constants.DAMAGE_ROCK_TRAP;
        this.cooldown = Constants.COOLDOWN_ROCK_TRAP;
        this.cooldownActiveTrap = Constants.COOLDOWN_STATUS_ACTIVE_ROCK_TRAP;
        this.value = Constants.VALUE_ROCK_TRAP;

    }
}
