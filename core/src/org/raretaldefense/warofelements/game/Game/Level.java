package org.raretaldefense.warofelements.game.Game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Queue;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Game.GameObjects.AbstractGameObject;
import org.raretaldefense.warofelements.game.Game.GameObjects.Enemies.AbstractEnemy;
import org.raretaldefense.warofelements.game.Game.GameObjects.Enemies.FireEnemy;
import org.raretaldefense.warofelements.game.Game.GameObjects.Enemies.NormalEnemy;
import org.raretaldefense.warofelements.game.Game.GameObjects.Enemies.WaterEnemy;
import org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles.AbstractProjectile;
import org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles.FireProjectile;
import org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles.RockProjectile;
import org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles.ThunderProjectile;
import org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles.WaterProjectile;
import org.raretaldefense.warofelements.game.Game.GameObjects.Projectiles.WindProjectile;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.AbstractTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.FireTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.ThunderTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.WaterTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.WindTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.AbstractTrap;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.ConfusionTrap;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.RockTrap;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.Trapdoor;
import org.raretaldefense.warofelements.game.Helpers.MapHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class Level implements Disposable {

    private final String TAG = getClass().getSimpleName();

    //Resources of level
    public TiledMap tiledMap;
    public UserInterface gui;


    //Enemies
    public List<AbstractGameObject> gameObjectsStructure;
    public List<AbstractTower> listOfTowersInGame;
    public List<AbstractTrap> listOfTrapsInGame;
    public List<AbstractEnemy> listOfEnemiesInGame, listOfEnemiesToDelete;
    public Queue<AbstractEnemy> listOfEnemiesToSpawn;
    public Queue<Queue<AbstractEnemy>> hordesReady;
    public List<AbstractProjectile> listOfProjectilesInGame, listOfProjectilesToDelete;
    private int limitRounds;
    private int limitTimeForRound;
    private int currentRound;
    private float currentTime;
    private Random aleatoryFactor;
    private int numberOfNewEnemies;
    public boolean isRoundStarted;
    public LevelState levelState;
    public int numberOfEnemiesInCurrentRound;

    //Variables of MapHelper
    private Vector2 cellRespawn;
    private Vector2 coordenatesRespawn;


    /*
    *
    * For control the interactions of user, we think that the best way is have a
    * string that identify if the player will build a tower/trap (ex: "toweFire") or
    * click in the grass (ex: "null").
    *
    * If the user will can look the information of a tower or he just want to find a
    * bugs in the construction mode, this coordenates will go to a HashMap that contains
    * all the coordenates (keys) with his towers/traps(value).
    *
    */
    //Resources of game
    public String userTarget;
    public boolean isGamePaused;
    public HashMap<Vector2, AbstractGameObject> mapOfStructures;


    public enum LevelState {
        GENERATING, WAITING, SPAWNING
    }


    public Level(String level) {
        init(level);
    }


    public void init(String level) {

        //Game
        isGamePaused = false;


        //TiledMap
        if (Gdx.app.getType().equals(Application.ApplicationType.Desktop)) {
            this.tiledMap = new TmxMapLoader().load(Gdx.files.absolute(Constants.PATH_LEVELS_DESKTOP + "_" + level + ".tmx").path());
        } else {
            this.tiledMap = new TmxMapLoader().load(Constants.PATH_LEVELS + "_" + level + ".tmx");
        }
        MapHelper.instance.init(tiledMap);
        MapHelper.instance.readMap();
        cellRespawn = MapHelper.instance.cellRespawn;
        coordenatesRespawn = MapHelper.instance.calculatePixelsPositionCell(cellRespawn);


        //Level
        aleatoryFactor = new Random(System.nanoTime());
        limitRounds = MapHelper.instance.getNumberRounds();
        limitTimeForRound = Constants.LEVEL_TIME_ROUND;
        currentRound = 0;
        currentTime = 0;
        gameObjectsStructure = new ArrayList<AbstractGameObject>();
        isRoundStarted = false;

        //Gui
        gui = new UserInterface(aleatoryFactor);
        gui.setAmountMoney(MapHelper.instance.getStarterCoins());
        userTarget = null;

        //Enemies
        levelState = LevelState.GENERATING;
        numberOfEnemiesInCurrentRound = MapHelper.instance.getStarterEnemies();
        listOfEnemiesToSpawn = new Queue<AbstractEnemy>();
        listOfEnemiesInGame = new ArrayList<AbstractEnemy>();
        listOfEnemiesToDelete = new ArrayList<AbstractEnemy>();
        hordesReady = new Queue<Queue<AbstractEnemy>>();

        //Structures
        listOfTowersInGame = new LinkedList<AbstractTower>();
        listOfTrapsInGame = new LinkedList<AbstractTrap>();
        mapOfStructures = new HashMap<Vector2, AbstractGameObject>();
        listOfProjectilesInGame = new LinkedList<AbstractProjectile>();
        listOfProjectilesToDelete = new ArrayList<AbstractProjectile>();


    }


    public void update(float delta) {


        gui.update(delta);

        if (isRoundStarted) {
            controlEnemies(delta);
        }


        for (AbstractTrap currentTrap : listOfTrapsInGame) {

            currentTrap.update(delta);

            if (currentTrap instanceof RockTrap) {
                if (!currentTrap.isItCanBeUsed() && currentTrap.getShoots() <= 1) {
                    AbstractProjectile rockProjectile = new RockProjectile(new Vector2(0, 0), currentTrap);
                    listOfProjectilesInGame.add(rockProjectile);
                }
            } else if (currentTrap instanceof ConfusionTrap) {
                if (currentTrap.isActive()) {
                    for (AbstractEnemy enemy : listOfEnemiesInGame) {

                        if (!enemy.isConfused() && enemy.getPositionTiledMap().x == currentTrap.getPositionTiledMap().x
                                && enemy.getPositionTiledMap().y == currentTrap.getPositionTiledMap().y) {
                            Vector2 positionOrigenConfused = MapHelper.instance.calculatePixelsPositionCell(currentTrap.getPositionTiledMap());
                            positionOrigenConfused.x += Constants.SIZE_CELL_WIDTH / 2f - enemy.getDimension().x / 2f;
                            positionOrigenConfused.y += Constants.SIZE_CELL_HEIGHT / 2f - enemy.getDimension().y / 2f;
                            enemy.setPositionScreen(new Vector2(positionOrigenConfused.x, positionOrigenConfused.y));
                            enemy.setPositionTiledMap(new Vector2(currentTrap.getPositionTiledMap()));
                            enemy.setIsConfused(true);
                            enemy.setCount(0);

                        }
                    }
                }
            } else if (currentTrap instanceof Trapdoor) {
                if (currentTrap.isActive()) {
                    for (AbstractEnemy currentEnemy : listOfEnemiesInGame) {
                        if (currentEnemy.getPositionTiledMap().x == currentTrap.getPositionTiledMap().x &&
                                currentEnemy.getPositionTiledMap().y == currentTrap.getPositionTiledMap().y) {
                            currentEnemy.setIsDeadForStructure(true);
                        }
                    }
                }
            }
        }

        for (AbstractTower currentTower : listOfTowersInGame) {

            currentTower.update(delta);

            if (!currentTower.isTargetVisible()) {
                currentTower.asignEnemy(listOfEnemiesInGame);
            }

            if (currentTower.isCanShoot() && currentTower.getTarget() != null) {
                if (currentTower instanceof FireTower) {
                    AbstractProjectile fireProjectile = new FireProjectile(currentTower, currentTower.getTarget());
                    listOfProjectilesInGame.add(fireProjectile);
                } else if (currentTower instanceof WaterTower) {
                    AbstractProjectile waterProjectile = new WaterProjectile(currentTower, currentTower.getTarget());
                    listOfProjectilesInGame.add(waterProjectile);
                } else if (currentTower instanceof ThunderTower) {
                    AbstractProjectile thunderProjectile = new ThunderProjectile(currentTower, currentTower.getTarget());
                    listOfProjectilesInGame.add(thunderProjectile);
                } else if (currentTower instanceof WindTower) {
                    AbstractProjectile windProjectile = new WindProjectile(currentTower, currentTower.getTarget());
                    listOfProjectilesInGame.add(windProjectile);
                }
            }
        }

        for (AbstractEnemy currentEnemy : listOfEnemiesInGame) {

            currentEnemy.update(delta);

            //If enemy died for a tower or a trap, then we give coins and score to player
            if (currentEnemy.isDeadForStructure()) {
                listOfEnemiesToDelete.add(currentEnemy);
                gui.setAmounScore(Constants.SCORE_ENEMY);
                gui.setAmountMoney(Constants.COINS_ENEMY);

            } else if (currentEnemy.isDead()) {  // Else we remove hearths to player
                gui.diminishHearts(currentEnemy.getDamage());
                if (gui.isPlayerDead()) { //If player lose all his hearths then he lose the game
                    gui.activatinLoseAnimation();
                    isGamePaused = true;
                }
                listOfEnemiesToDelete.add(currentEnemy);
            }
        }


        for (AbstractProjectile currentProjectile : listOfProjectilesInGame) {

            currentProjectile.update(delta);

            if (currentProjectile.isImpacted()) {

                int realDamage = currentProjectile.getDamage();
                AbstractEnemy enemyTarget = currentProjectile.getEnemy();


                if (currentProjectile instanceof WindProjectile) {

                    //Set the correct state
                    if (enemyTarget instanceof FireEnemy) {
                        enemyTarget.setState(Constants.STATE_ENEMY.FAST);

                    } else if (enemyTarget instanceof WaterEnemy || enemyTarget instanceof NormalEnemy) {
                        enemyTarget.setState(Constants.STATE_ENEMY.SLOW);
                    }
                    //Reapply the effect
                    enemyTarget.setCountState(0);

                } else if (currentProjectile instanceof FireProjectile && enemyTarget instanceof NormalEnemy ||
                        currentProjectile instanceof ThunderProjectile && enemyTarget instanceof WaterEnemy ||
                        currentProjectile instanceof WaterProjectile && enemyTarget instanceof FireEnemy) {

                    realDamage *= Constants.BONUS_DAMAGE;

                } else if (currentProjectile instanceof FireProjectile && (enemyTarget instanceof WaterEnemy || enemyTarget instanceof FireEnemy) ||
                        currentProjectile instanceof ThunderProjectile && enemyTarget instanceof NormalEnemy ||
                        currentProjectile instanceof WaterProjectile && enemyTarget instanceof WaterEnemy) {

                    realDamage /= Constants.BONUS_DAMAGE;

                }


                if (currentProjectile instanceof RockProjectile) {

                    Ellipse area = new Ellipse(currentProjectile.getRockTrap().getPositionScreen().x,
                            currentProjectile.getRockTrap().getPositionScreen().y,
                            MapHelper.instance.getWidthCellInPixels() * 1.5f, MapHelper.instance.getHeightCellInPixels() * 2);
                    for (AbstractEnemy enemy : listOfEnemiesInGame) {

                        if (area.contains(enemy.getPositionScreen().x, enemy.getPositionScreen().y)) {
                            enemy.calculateHealthAfterImpact(currentProjectile.getDamage());
                        }
                    }
                } else {
                    currentProjectile.getEnemy().calculateHealthAfterImpact(realDamage);

                    if (currentProjectile.getEnemy() == null || currentProjectile.getEnemy().isDeadForStructure() || currentProjectile.getEnemy().isDead()) {
                        currentProjectile.getTower().setTarget(null);
                    }
                }
                listOfProjectilesToDelete.add(currentProjectile);
            }
        }

        if (!listOfEnemiesToDelete.isEmpty()) {
            listOfEnemiesInGame.removeAll(listOfEnemiesToDelete);
            listOfEnemiesToDelete.clear();
        }

        if (!listOfProjectilesToDelete.isEmpty()) {
            listOfProjectilesInGame.removeAll(listOfProjectilesToDelete);
            listOfProjectilesToDelete.clear();
        }


    }


    private void controlEnemies(float deltaTime) {

        currentTime += deltaTime;

        //When the lever are generating unities
        if (levelState.equals(LevelState.GENERATING)) {

            //First calculate the total of enemies in the round
            numberOfEnemiesInCurrentRound = calculateNewNumberOfEnemies(numberOfEnemiesInCurrentRound);
            numberOfNewEnemies = numberOfEnemiesInCurrentRound;

            Gdx.app.debug(TAG, "Round " + currentRound + "/" + limitRounds + " - Number of enemies -> " + numberOfEnemiesInCurrentRound);


            //Then we make a groups of enemies that are the same type, this looks like the enemies are "friends" and go together in herd
            while (numberOfNewEnemies > 0) {

                int enemiesSelected = aleatoryFactor.nextInt(2) + 3; //Groups beetwen 3 and 5

                numberOfNewEnemies -= enemiesSelected; //Less total
                int category = aleatoryFactor.nextInt(3); //Type aleatory
                Queue<AbstractEnemy> reclutingHordes = new Queue<AbstractEnemy>();

                //We asigning the parameters to the all members of the group
                for (int y = 0; y < enemiesSelected; y++) {

                    AbstractEnemy currentEnemy = null;

                    Vector2 pixelsRespawn = new Vector2(coordenatesRespawn);
                    pixelsRespawn.x += aleatoryFactor.nextInt(Constants.SIZE_CELL_WIDTH - 20);
                    pixelsRespawn.y += aleatoryFactor.nextInt(Constants.SIZE_CELL_HEIGHT - 10);

                    switch (category) {

                        case Constants.NORMAL_ENEMY:
                            currentEnemy = new NormalEnemy(new Vector2(cellRespawn), pixelsRespawn, null, null, new Vector2(1, 1), 0);
                            break;
                        case Constants.WATER_ENEMY:
                            currentEnemy = new WaterEnemy(new Vector2(cellRespawn), pixelsRespawn, null, null, new Vector2(1, 1), 0);
                            break;
                        case Constants.FIRE_ENEMY:
                            currentEnemy = new FireEnemy(new Vector2(cellRespawn), pixelsRespawn, null, null, new Vector2(1, 1), 0);
                            break;


                    }

                    //And finally, we add a bit of difficulty to player adding more life to enemies in function of total enemies in the round
                    currentEnemy.setHealth(currentEnemy.getHealth() + numberOfEnemiesInCurrentRound);
                    reclutingHordes.addLast(currentEnemy);


                }

                hordesReady.addLast(reclutingHordes);

            }

            levelState = LevelState.SPAWNING;
        }

        //Next, we go to "spawn" enemies to the land
        if (levelState.equals(LevelState.SPAWNING)) {

            //Time beetwen units are more accelerate when the last round are more
            float timeBetweenUnitsToSpawn = 1 / Constants.LEVEL_FREQUENCY_UNITS * (limitRounds - currentRound / 2 + 1);

            if (hordesReady.size < numberOfEnemiesInCurrentRound / (4 * 3)) {
                timeBetweenUnitsToSpawn /= 2;
            }

            if (currentTime > timeBetweenUnitsToSpawn) {

                if (listOfEnemiesToSpawn.size == 0) {

                    if (hordesReady.size > 0) {
                        listOfEnemiesToSpawn = hordesReady.removeFirst();
                    } else {
                        currentTime = 0;
                        levelState = LevelState.WAITING;
                    }
                } else {
                    listOfEnemiesInGame.add(listOfEnemiesToSpawn.removeFirst());
                    currentTime -= timeBetweenUnitsToSpawn;
                }

            }

        } else if (levelState.equals(LevelState.WAITING)) {

            if (currentTime > limitTimeForRound || (listOfEnemiesInGame.isEmpty() && currentTime / 10 > limitTimeForRound)) {

                if (currentRound < limitRounds) {

                    currentTime = 0;
                    currentRound++;
                    numberOfNewEnemies++;
                    levelState = LevelState.GENERATING;

                } else if (listOfEnemiesInGame.isEmpty()) {
                    isGamePaused = true;
                    gui.activatingWinAnimation();

                }


            }


        }

    }

    public void render(SpriteBatch batch) {


        batch.begin();

        //Render all the AbstractGameObjects
        for (AbstractTrap currentTrap : listOfTrapsInGame) {
            currentTrap.render(batch);
        }

        for (AbstractTower currentTower : listOfTowersInGame) {
            currentTower.render(batch);
        }

        for (AbstractEnemy currentEnemy : listOfEnemiesInGame) {
            currentEnemy.render(batch);
        }

        for (AbstractProjectile currentProjectile : listOfProjectilesInGame) {
            currentProjectile.render(batch);
        }


        batch.end();


        gui.render();


    }

    /**
     * This method calculate the next number in the sequence of Fibonacci that we used as
     * a algorith from number base of enemies. Next, we apply a little random factor for make it
     * more funny.
     *
     * @param actualNumber
     * @return
     */
    public int calculateNewNumberOfEnemies(int actualNumber) {

        int n_prev = 0, n_current = 1, n_post;

        while (n_current <= actualNumber) {
            n_post = n_current + n_prev;
            n_prev = n_current;
            n_current = n_post;
        }

        n_current += aleatoryFactor.nextInt(n_prev / 4);


        return n_current;
    }


    @Override
    public void dispose() {

    }


}
