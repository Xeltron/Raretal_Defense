package org.raretaldefense.warofelements.game.Game;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;
import org.raretaldefense.warofelements.game.Helpers.MapHelper;

import java.util.Random;


public class UserInterface {


    private final String TAG = WorldController.class.getSimpleName();

    private Stage stageGUI;
    private Image coin, heart, orbFire, orbWater, orbWind, orbThunder, buttonReverse, imageDetail, coinPrice, trapRock, trapdoor, trapConfusion, detailTopImage, detailCenterImage, detailBottomImage, buttonPause, filterBlack, enemyTroll, starLeft, starCenter, starRight, imgGame, imgOver, gameOverEnemyLeft, gameOverEnemyRight;
    private ImageButton buttonBuild, buttonEvolve, buttonSell, buttonStartRound, buttonResume, buttonBackToMenu, buttonExit, buttonBackToMenuInGameOver;
    private Label coinText, scoreText, heartText, detailTop, detailCenter, detailBottom, detailPrice, finalScoreText;
    private int coinValue, scoreValue, heartValue;
    private float scoreAnimatedWin;
    private float padding;
    public boolean playerWantGoMenu, playerWantLeave, playerWinLevel, playerLoseLevel;
    private TextureRegionDrawable damageDetail, rangeDetail, timeDetail, buttonShowConstructionPanel, buttonHideConstructionPanel, buttonCancel, confusionDetail, textureStarOn, textureStarOff;
    private Random random;

    public PanelState panelState, panelDetailState, pauseState, enemiesGameOverState;

    private Table tableConstruction, tableDetail, tableIndicators, tableTraps;


    private enum PanelState {
        UP, DOWN, RISE_UP, RISE_DOWN
    }


    public UserInterface(Random random) {
        this.random = random;
        init();
    }


    public void init() {

        //==================================
        //      INITIALIZING VARIABLES
        //==================================


        //Values
        coinValue = 0;
        scoreValue = 0;
        heartValue = Constants.LEVEL_TOTAL_LIVES;
        padding = Gdx.graphics.getWidth() / 100f;


        stageGUI = new Stage();


        //Styles and Textures
        Label.LabelStyle scoresStyle = new Label.LabelStyle(Assets.instance.fonts.fontBig, Color.WHITE);
        Label.LabelStyle detailStyle = new Label.LabelStyle(Assets.instance.fonts.fontMedium, Color.BLACK);
        TextureRegionDrawable woodBackground = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_WOOD_PANEL));
        TextureRegionDrawable woodBackgroundLateral = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_WOOD_SIGN));
        TextureRegionDrawable textureButtonConstructionUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_CONSTRUCTION_UP));
        TextureRegionDrawable textureButtonConstructionDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_CONSTRUCTION_DOWN));
        TextureRegionDrawable textureButtonEvolveUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_EVOLVE_UP));
        TextureRegionDrawable textureButtonEvolveDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_EVOLVE_DOWN));
        TextureRegionDrawable textureButtonSellUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_SELL_UP));
        TextureRegionDrawable textureButtonSellDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_SELL_DOWN));
        TextureRegionDrawable textureButtonStartRoundUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_START_ROUND_UP));
        TextureRegionDrawable textureButtonStartRoundDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_START_ROUND_DOWN));
        TextureRegionDrawable textureButtonResumeUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_RESUME_UP));
        TextureRegionDrawable textureButtonResumeDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_RESUME_DOWN));
        TextureRegionDrawable textureButtonBackToMenuUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_BACK_MENU_UP));
        TextureRegionDrawable textureButtonBackToMenuDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_BACK_MENU_DOWN));
        TextureRegionDrawable textureButtonExitUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_EXIT_UP));
        TextureRegionDrawable textureButtonExitDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_EXIT_DOWN));
        textureStarOn = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_STAR_ON));
        textureStarOff = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_STAR_OFF));

        buttonShowConstructionPanel = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_SHOW_PANEL_CONSTRUCTION));
        buttonHideConstructionPanel = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_HIDE_PANEL_CONSTRUCTION));
        buttonCancel = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_CANCEL));
        damageDetail = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_DETAIL_DAMAGE));
        rangeDetail = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_DETAIL_RANGE));
        timeDetail = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegions(Constants.REFERENCE_NAME_ATLAS_COOLDOWN_TRAPS).get(2));
        confusionDetail = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_DETAIL_CONFUSION));
        //confusionDetail = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegions(Constants.REFERENCE_NAME_ATLAS_COOLDOWN_TRAPS).get(2));


        //Widgets
        coin = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_COIN));
        heart = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_HEART));
        coinText = new Label(formatNumber(coinValue, Constants.NUMBER_DIGITS_MONEY), scoresStyle);
        scoreText = new Label(formatNumber(scoreValue, Constants.NUMBER_DIGITS_SCORE), scoresStyle);
        heartText = new Label(String.valueOf(heartValue), scoresStyle);
        finalScoreText = new Label(formatNumber(0, Constants.NUMBER_DIGITS_SCORE), scoresStyle);
        detailTop = new Label("0", detailStyle);
        detailCenter = new Label("0", detailStyle);
        detailBottom = new Label("0", detailStyle);
        detailPrice = new Label("0", detailStyle);
        orbFire = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_FIRE_ORB));
        orbWater = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_WATER_ORB));
        orbThunder = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_THUNDER_ORB));
        orbWind = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_WIND_ORB));
        trapRock = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_ROCK_TRAP_ORB));
        trapdoor = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_TRAPDOOR_CLOSE));
        trapConfusion = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_CONFUSION_TRAP_ORB));
        buttonReverse = new Image(buttonHideConstructionPanel);
        coinPrice = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_COIN));
        filterBlack = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_FILTER_BLACK));
        starLeft = new Image(textureStarOff);
        starCenter = new Image(textureStarOff);
        starRight = new Image(textureStarOff);
        imgGame = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_GAME));
        imgOver = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_OVER));
        gameOverEnemyLeft = new Image();
        gameOverEnemyRight = new Image();
        imageDetail = new Image();
        detailTopImage = new Image();
        detailCenterImage = new Image();
        detailBottomImage = new Image();
        buttonPause = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_PAUSE));
        buttonBuild = new ImageButton(textureButtonConstructionUp, textureButtonConstructionDown);
        buttonEvolve = new ImageButton(textureButtonEvolveUp, textureButtonEvolveDown);
        buttonSell = new ImageButton(textureButtonSellUp, textureButtonSellDown);
        buttonStartRound = new ImageButton(textureButtonStartRoundUp, textureButtonStartRoundDown);
        buttonResume = new ImageButton(textureButtonResumeUp, textureButtonResumeDown);
        buttonBackToMenu = new ImageButton(textureButtonBackToMenuUp, textureButtonBackToMenuDown);
        buttonBackToMenuInGameOver = new ImageButton(textureButtonBackToMenuUp, textureButtonBackToMenuDown);
        buttonExit = new ImageButton(textureButtonExitUp, textureButtonExitDown);


        //==================================
        //  CONFIGURING TABLES AND WIDGETS
        //==================================

        //Button show/hide Panel construction
        buttonReverse.setSize(Gdx.graphics.getWidth() / 10f, Gdx.graphics.getHeight() / 10f);
        buttonReverse.setPosition(padding * 2, Gdx.graphics.getHeight() / 4f - buttonReverse.getHeight() / 5f);

        //Button pause
        buttonPause.setSize(Gdx.graphics.getWidth() / 10f, Gdx.graphics.getHeight() / 10f);
        buttonPause.setPosition(Gdx.graphics.getWidth() - buttonPause.getWidth() - padding * 2, -buttonPause.getHeight());


        //Button start round
        buttonStartRound.setSize(Gdx.graphics.getWidth() / 3f, Gdx.graphics.getHeight() / 10f);
        buttonStartRound.setPosition(Gdx.graphics.getWidth() / 2f - buttonStartRound.getWidth() / 2f, Gdx.graphics.getHeight() - buttonStartRound.getHeight());


        //------
        //Table of indicators of the game in top
        tableIndicators = new Table();
        tableIndicators.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 10f);
        tableIndicators.setPosition(0, Gdx.graphics.getHeight() - tableIndicators.getHeight());
        tableIndicators.top();

        //Image coin
        coin.setSize(tableIndicators.getHeight(), tableIndicators.getHeight());
        coin.setX(padding);

        //Lable coins
        resizeFontText(coinText, tableIndicators, .1f, .5f);
        coinText.setPosition(coin.getX() + coin.getWidth() + padding, tableIndicators.getHeight() / 2f - coinText.getHeight() / 2f);

        //Label score
        resizeFontText(scoreText, tableIndicators, .2f, .5f);
        scoreText.setPosition(tableIndicators.getWidth() / 2f - (scoreText.getWidth() / 2f), tableIndicators.getHeight() / 2f - scoreText.getHeight() / 2f);

        //Label hearths
        resizeFontText(heartText, tableIndicators, .1f, .5f);
        heartText.setPosition(tableIndicators.getWidth() - heartText.getWidth() - padding, tableIndicators.getHeight() / 2f - scoreText.getHeight() / 2f);

        //Image hearth
        heart.setSize(tableIndicators.getHeight(), tableIndicators.getHeight());
        heart.setX(tableIndicators.getWidth() - heart.getWidth() - (tableIndicators.getWidth() - heartText.getX()) - padding);

        //------
        //Construction Panel
        tableConstruction = new Table();
        tableConstruction.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 4f);
        tableConstruction.background(woodBackground);

        //Orb Fire
        orbFire.setSize(tableConstruction.getHeight() - padding * 4, tableConstruction.getHeight() - padding * 4);
        orbFire.setPosition(tableConstruction.getWidth() / 10f, padding * 2);

        //Orb Water
        orbWater.setSize(tableConstruction.getHeight() - padding * 4, tableConstruction.getHeight() - padding * 4);
        orbWater.setPosition(orbFire.getX() + orbFire.getWidth() + padding, padding * 2);

        //Orb Thunder
        orbThunder.setSize(tableConstruction.getHeight() - padding * 4, tableConstruction.getHeight() - padding * 4);
        orbThunder.setPosition(orbWater.getX() + orbWater.getWidth() + padding, padding * 2);

        //Orb Wind
        orbWind.setSize(tableConstruction.getHeight() - padding * 4, tableConstruction.getHeight() - padding * 4);
        orbWind.setPosition(orbThunder.getX() + orbThunder.getWidth() + padding, padding * 2);


        //Subtable in construction panel for traps
        tableTraps = new Table();
        tableTraps.setPosition(orbWind.getX() + orbWind.getWidth() + padding * 2, padding * 2);
        tableTraps.setSize(tableConstruction.getWidth() - tableTraps.getX() - tableConstruction.getWidth() / 10f, tableConstruction.getHeight() - padding * 4);

        //If is large, then we put the traps horizontally
        if (Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight() > Constants.FRONTEIR_TYPES_OF_SCREEN) {

            //TrapRock
            trapRock.setSize(tableTraps.getWidth() / 3f - padding * 4, tableTraps.getWidth() / 3f - padding * 4);
            trapRock.setPosition(padding, padding);
            //Trapdoor
            trapdoor.setSize(tableTraps.getWidth() / 3f - padding * 4, tableTraps.getWidth() / 3f - padding * 4);
            trapdoor.setPosition(trapRock.getX() + trapRock.getWidth() + padding * 2, padding);
            //TrapConfusion
            trapConfusion.setSize(tableTraps.getWidth() / 3f - padding * 4, tableTraps.getWidth() / 3f - padding * 4);
            trapConfusion.setPosition(trapdoor.getX() + trapdoor.getWidth() + padding * 2, padding);

            tableTraps.addActor(trapRock);
            tableTraps.addActor(trapdoor);
            tableTraps.addActor(trapConfusion);


        } else { //Otherwise, we put the traps vertically

            //We can put it easier than the other way
            tableTraps.add(trapRock).padBottom(padding);
            tableTraps.row();
            tableTraps.add(trapdoor).padBottom(padding);
            tableTraps.row();
            tableTraps.add(trapConfusion);
        }


        //Table details
        tableDetail = new Table();
        tableDetail.setPosition(Gdx.graphics.getWidth(), tableConstruction.getHeight());
        tableDetail.setSize(Gdx.graphics.getWidth() / 3f, Gdx.graphics.getHeight() - tableConstruction.getHeight());
        tableDetail.background(woodBackgroundLateral);

        //Image from current detail
        imageDetail.setSize(tableDetail.getWidth() / 4f, tableDetail.getWidth() / 4f);
        imageDetail.setPosition(tableDetail.getWidth() / 2f - imageDetail.getWidth() / 2f, tableDetail.getHeight() - imageDetail.getHeight() - padding * 7);


        //Detail Top
        resizeFontText(detailTop, tableDetail, .05f, .05f);
        detailTopImage.setSize(tableDetail.getHeight() / 10f, tableDetail.getHeight() / 10f);
        detailTopImage.setPosition(tableDetail.getWidth() / 3f - detailTopImage.getWidth() / 2f - detailTop.getWidth() / 2f + padding, imageDetail.getY() - detailTopImage.getHeight() - padding);
        detailTop.setPosition(detailTopImage.getX() + detailTopImage.getWidth(), detailTopImage.getY() + detailTop.getHeight() / 2f);

        //Detail Center
        resizeFontText(detailCenter, tableDetail, .05f, .05f);
        detailCenterImage.setSize(tableDetail.getHeight() / 10, tableDetail.getHeight() / 10);
        detailCenterImage.setPosition(tableDetail.getWidth() / 3 - detailCenterImage.getWidth() / 2 - detailCenter.getWidth() / 2 + padding, detailTopImage.getY() - detailCenterImage.getHeight() - padding);
        detailCenter.setPosition(detailCenterImage.getX() + detailCenterImage.getWidth(), detailCenterImage.getY() + detailCenter.getHeight() / 2);

        //Detail Bottom
        resizeFontText(detailBottom, tableDetail, .05f, .05f);
        detailBottomImage.setSize(tableDetail.getHeight() / 10, tableDetail.getHeight() / 10);
        detailBottomImage.setPosition(tableDetail.getWidth() / 3 - detailBottomImage.getWidth() / 2 - detailBottom.getWidth() / 2 + padding, detailCenterImage.getY() - detailBottomImage.getHeight() - padding);
        detailBottom.setPosition(detailBottomImage.getX() + detailBottomImage.getWidth(), detailBottomImage.getY() + detailBottom.getHeight() / 2);


        //Buttons
        buttonBuild.setBounds(tableDetail.getWidth() / 2 - tableDetail.getWidth() / 4,
                tableDetail.getHeight() / 10,
                tableDetail.getWidth() / 2, tableDetail.getHeight() / 10);

        //Next 2 buttons just are initzialized but not add to table, because the first time can't show a existing tower
        buttonEvolve.setBounds(tableDetail.getWidth() / 2 - tableDetail.getWidth() / 4,
                tableDetail.getHeight() / 10,
                tableDetail.getWidth() / 4, tableDetail.getHeight() / 10);

        buttonSell.setBounds(tableDetail.getWidth() / 2,
                tableDetail.getHeight() / 10,
                tableDetail.getWidth() / 4, tableDetail.getHeight() / 10);


        //Price of build or evolve
        resizeFontText(detailPrice, tableDetail, .05f, .05f);
        coinPrice.setSize(detailPrice.getHeight(), detailPrice.getHeight());
        coinPrice.setPosition(tableDetail.getWidth() / 3 - coinPrice.getWidth() / 2 - detailPrice.getWidth() / 2 + padding * 3, buttonBuild.getY() + buttonBuild.getHeight() + padding);
        detailPrice.setPosition(coinPrice.getX() + coinPrice.getWidth() + padding, coinPrice.getY());


        //==================================
        //             PAUSE
        //==================================

        //Filter Black from better nice pause screen
        filterBlack.setPosition(0, 0);
        filterBlack.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        filterBlack.setVisible(false);


        //Buttons from pause
        buttonResume.setSize(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 5);
        buttonResume.setPosition(-buttonResume.getWidth(), Gdx.graphics.getHeight() / 2 + buttonResume.getHeight());
        buttonResume.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                resumingGame();
                super.touchUp(event, x, y, pointer, button);
            }
        });


        buttonBackToMenu.setSize(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 5);
        buttonBackToMenu.setPosition(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2);
        buttonBackToMenu.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                playerWantGoMenu = true;
                super.touchUp(event, x, y, pointer, button);
            }
        });

        buttonExit.setSize(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 5);
        buttonExit.setPosition(-buttonExit.getWidth(), Gdx.graphics.getHeight() / 2 - buttonExit.getHeight());
        buttonExit.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {


                enemyTroll = selectRandomEnemy();

                enemyTroll.setSize(1, 1);
                enemyTroll.setPosition(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);
                stageGUI.addActor(enemyTroll);
                playerWantLeave = true;

                buttonResume.setDisabled(true);
                buttonBackToMenu.setDisabled(true);
                buttonExit.setDisabled(true);


                super.touchUp(event, x, y, pointer, button);
            }
        });


        //==================================
        //             WIN
        //==================================

        //Star left
        starLeft.setSize(Gdx.graphics.getWidth() / 7, Gdx.graphics.getHeight() / 7);
        starLeft.setPosition(Gdx.graphics.getWidth() / 3 - starLeft.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        starLeft.setVisible(false);

        //Star center
        starCenter.setSize(Gdx.graphics.getWidth() / 7, Gdx.graphics.getHeight() / 7);
        starCenter.setPosition(Gdx.graphics.getWidth() / 2 - starCenter.getWidth() / 2, Gdx.graphics.getHeight() / 2 + padding * 2);
        starCenter.setVisible(false);

        //Star Right
        starRight.setSize(Gdx.graphics.getWidth() / 7, Gdx.graphics.getHeight() / 7);
        starRight.setPosition(Gdx.graphics.getWidth() * (2 / 3f) - starRight.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        starRight.setVisible(false);

        //Text score
        finalScoreText.setFontScale((Gdx.graphics.getWidth() / 3) / finalScoreText.getWidth(), (Gdx.graphics.getHeight() / 10) / finalScoreText.getHeight());
        finalScoreText.setSize(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 10);
        finalScoreText.setPosition(Gdx.graphics.getWidth() / 2 - finalScoreText.getWidth() / 2, Gdx.graphics.getHeight() * (2 / 3f));
        finalScoreText.setVisible(false);

        //Button back to Menu from Game Over
        buttonBackToMenuInGameOver.setSize(Gdx.graphics.getWidth() / 3, Gdx.graphics.getHeight() / 5);
        buttonBackToMenuInGameOver.setPosition(Gdx.graphics.getWidth() / 2 - buttonBackToMenuInGameOver.getWidth() / 2, Gdx.graphics.getHeight() / 3 - buttonBackToMenuInGameOver.getHeight() / 2);
        buttonBackToMenuInGameOver.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                playerWantGoMenu = true;
                super.touchUp(event, x, y, pointer, button);
            }
        });
        buttonBackToMenuInGameOver.setVisible(false);


        //==================================
        //             LOSE
        //==================================

        //Text "Game"
        imgGame.setSize(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 3);
        imgGame.setPosition(-imgGame.getWidth(), Gdx.graphics.getHeight() / 2);
        imgGame.setVisible(false);

        //Text "Lose"
        imgOver.setSize(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 3);
        imgOver.setPosition(Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 2);
        imgOver.setVisible(false);

        //Enemy left
        gameOverEnemyLeft = selectRandomEnemy();
        gameOverEnemyLeft.setSize(Gdx.graphics.getWidth() / 5, Gdx.graphics.getHeight() / 5);
        gameOverEnemyLeft.setPosition(buttonBackToMenuInGameOver.getX() - gameOverEnemyLeft.getWidth() - padding * 2, buttonBackToMenuInGameOver.getHeight());
        gameOverEnemyLeft.setVisible(false);

        //Enemy right
        gameOverEnemyRight = selectRandomEnemy();
        gameOverEnemyRight.setSize(Gdx.graphics.getWidth() / 5, Gdx.graphics.getHeight() / 5);
        gameOverEnemyRight.setPosition(buttonBackToMenuInGameOver.getX() + buttonBackToMenuInGameOver.getWidth() + padding * 2, buttonBackToMenuInGameOver.getHeight());
        gameOverEnemyRight.setVisible(false);


        //==================================
        //         ADDING ACTORS
        //==================================


        //Table of coins,scores and hearths
        tableIndicators.addActor(coin);
        tableIndicators.addActor(coinText);
        //tableIndicators.addActor(scoreText);
        tableIndicators.addActor(heart);
        tableIndicators.addActor(heartText);


        //Table construction
        tableConstruction.addActor(orbFire);
        tableConstruction.addActor(orbWater);
        tableConstruction.addActor(orbThunder);
        tableConstruction.addActor(orbWind);
        tableConstruction.addActor(tableTraps);


        //Table detail
        tableDetail.addActor(imageDetail);
        tableDetail.addActor(detailTopImage);
        tableDetail.addActor(detailTop);
        tableDetail.addActor(detailCenterImage);
        tableDetail.addActor(detailCenter);
        tableDetail.addActor(detailBottomImage);
        tableDetail.addActor(detailBottom);
        tableDetail.addActor(buttonBuild);
        tableDetail.addActor(coinPrice);
        tableDetail.addActor(detailPrice);

        //Add all elements to stage in priority order
        stageGUI.addActor(tableDetail);
        stageGUI.addActor(buttonPause);
        stageGUI.addActor(tableConstruction);
        stageGUI.addActor(buttonReverse);
        stageGUI.addActor(tableIndicators);
        stageGUI.addActor(buttonStartRound);
        stageGUI.addActor(filterBlack);
        stageGUI.addActor(buttonResume);
        stageGUI.addActor(buttonBackToMenu);
        stageGUI.addActor(buttonExit);
        stageGUI.addActor(starLeft);
        stageGUI.addActor(starCenter);
        stageGUI.addActor(starRight);
        stageGUI.addActor(finalScoreText);
        stageGUI.addActor(imgGame);
        stageGUI.addActor(imgOver);
        stageGUI.addActor(buttonBackToMenuInGameOver);
        stageGUI.addActor(gameOverEnemyLeft);
        stageGUI.addActor(gameOverEnemyRight);


        panelState = PanelState.UP;
        panelDetailState = PanelState.DOWN;
        pauseState = PanelState.DOWN;
        enemiesGameOverState = PanelState.RISE_UP;

        playerWantGoMenu = false;
        playerWantLeave = false;


        Gdx.input.setInputProcessor(stageGUI);

    }


    public void update(float delta) {

        if (pauseState.equals(PanelState.DOWN)) {

            if (playerWinLevel) {
                playerIsWinning(delta);
            } else if (playerLoseLevel) {
                playerIsLosing(delta);
            } else if (panelState.equals(PanelState.RISE_UP)) {
                showPanelConstruction(delta);
            } else if (panelState.equals(PanelState.RISE_DOWN)) {
                hidePanelConstruction(delta);
            }

            if (panelDetailState.equals(PanelState.RISE_UP)) {
                showPanelDetail(delta);
            } else if (panelDetailState.equals(PanelState.RISE_DOWN)) {
                hidePanelDetail(delta);
            }
        } else {
            if (playerWantLeave) {
                playerIsLeaving(delta);
            } else if (pauseState.equals(PanelState.RISE_UP)) {
                enteringInPause(delta);
            } else if (pauseState.equals(PanelState.RISE_DOWN)) {
                gettingBackGame(delta);
            }
        }
    }


    public void render() {


        stageGUI.draw();
    }


    public void updateDetail(boolean newElement, int type, Drawable image, int value, float... params) {

        clearDetails();

        if (!isPanelDetailShowed()) {
            showingPanelDetail();
        }


        imageDetail.setDrawable(image);
        detailPrice.setText(String.valueOf(value));

        switch (type) {


            case Constants.TYPE_DETAIL_DEFAULT_TOWER:
                detailTopImage.setDrawable(damageDetail);
                detailCenterImage.setDrawable(rangeDetail);
                detailBottomImage.setDrawable(timeDetail);
                setAmountDetailTop(params[0], false);
                setAmountDetailCenter(params[1], false);
                setAmountDetailBottom(params[2], true);
                break;

            case Constants.TYPE_DETAIL_TOWER_WIND:
                detailTopImage.setDrawable(rangeDetail);
                detailCenterImage.setDrawable(timeDetail);
                detailBottomImage.setDrawable(null);
                setAmountDetailTop(params[0], false);
                setAmountDetailCenter(params[1], true);
                break;

            case Constants.TYPE_DETAIL_ROCK_TRAP: //TODO
                detailTopImage.setDrawable(damageDetail);
                detailCenterImage.setDrawable(timeDetail);
                detailBottomImage.setDrawable(null);
                setAmountDetailTop(params[0], false);
                setAmountDetailCenter(params[1], true);
                break;

            case Constants.TYPE_DETAIL_TRAPDOOR: //TODO
                detailTopImage.setDrawable(timeDetail);
                detailCenterImage.setDrawable(null);
                detailBottomImage.setDrawable(null);
                setAmountDetailTop(params[0], false);
                break;

            case Constants.TYPE_DETAIL_CONFUSION_TRAP: //TODO
                detailTopImage.setDrawable(timeDetail);
                detailCenterImage.setDrawable(confusionDetail);
                detailBottomImage.setDrawable(null);
                setAmountDetailTop(params[0], false);
                setAmountDetailCenter(params[1], true);
                break;

            default:
        }

        tableDetail.removeActor(buttonBuild);
        tableDetail.removeActor(buttonEvolve);
        tableDetail.removeActor(buttonSell);
        tableDetail.removeActor(coinPrice);
        tableDetail.removeActor(detailPrice);

        if (newElement) {
            tableDetail.addActor(buttonBuild);
            tableDetail.addActor(coinPrice);
            tableDetail.addActor(detailPrice);

        } else {
            if (value != 0) {
                tableDetail.addActor(buttonEvolve);
                tableDetail.addActor(coinPrice);
                tableDetail.addActor(detailPrice);
            }
            tableDetail.addActor(buttonSell);

        }


    }


    public void startGame() {
        stageGUI.getActors().removeValue(buttonStartRound, false);
        tableIndicators.addActor(scoreText);
    }


    private void showPanelConstruction(float delta) {

        float amount = delta * Constants.VELOCITY_PANELS;

        if (buttonPause.getY() + buttonPause.getHeight() > 0) {
            buttonPause.setPosition(buttonPause.getX(), buttonPause.getY() - amount);
        } else if (tableConstruction.getY() < 0) {

            if (tableConstruction.getY() + amount > 0) {
                tableConstruction.setY(0);
            } else {
                tableConstruction.setPosition(0, tableConstruction.getY() + amount);
            }

            buttonReverse.setY(tableConstruction.getY() + tableConstruction.getHeight() - buttonReverse.getHeight() / 5f);

            if (buttonReverse.getY() + buttonReverse.getHeight() / 5f < Gdx.graphics.getHeight() / 4f) {
                buttonReverse.setY(tableConstruction.getY() + tableConstruction.getHeight() - buttonReverse.getHeight() / 5f);
            }

        } else {


            buttonReverse.setDrawable(buttonHideConstructionPanel);

            panelState = PanelState.UP;
        }


    }

    private void hidePanelConstruction(float delta) {

        float amount = delta * Constants.VELOCITY_PANELS;

        if (tableConstruction.getY() + tableConstruction.getHeight() > 0) {
            tableConstruction.setPosition(0, tableConstruction.getY() - amount);


            if (buttonReverse.getY() > 0) {
                buttonReverse.setY(tableConstruction.getY() + tableConstruction.getHeight() - buttonReverse.getHeight() / 5);
            }


        } else if (buttonPause.getY() < 0) {

            if (buttonPause.getY() + amount > 0) {
                buttonPause.setPosition(buttonPause.getX(), 0);
            } else {
                buttonPause.setPosition(buttonPause.getX(), buttonPause.getY() + amount);
            }

        } else {
            if (!buttonReverse.getDrawable().equals(buttonCancel)) {
                buttonReverse.setDrawable(buttonShowConstructionPanel);
            }
            panelState = PanelState.DOWN;
        }

    }

    private void showPanelDetail(float delta) {

        if (tableDetail.getX() + tableDetail.getWidth() > Gdx.graphics.getWidth()) {
            tableDetail.setPosition(tableDetail.getX() - delta * Constants.VELOCITY_PANELS, tableDetail.getY());
        } else {
            panelDetailState = PanelState.UP;
        }
    }

    private void hidePanelDetail(float delta) {

        if (tableDetail.getX() < Gdx.graphics.getWidth()) {
            tableDetail.setPosition(tableDetail.getX() + delta * Constants.VELOCITY_PANELS, tableDetail.getY());
        } else {
            panelDetailState = PanelState.DOWN;
        }


    }

    private void enteringInPause(float delta) {

        float amount = delta * Constants.VELOCITY_PANELS * 3;

        if (buttonResume.getX() < Gdx.graphics.getWidth() / 2f - buttonResume.getWidth() / 2f) {

            if (buttonResume.getX() + amount > Gdx.graphics.getWidth() / 2f - buttonResume.getWidth() / 2f) {
                buttonResume.setPosition(Gdx.graphics.getWidth() / 2f - buttonResume.getWidth() / 2f, buttonResume.getY());
            } else {
                buttonResume.setPosition(buttonResume.getX() + amount, buttonResume.getY());
            }

        } else if (buttonBackToMenu.getX() > Gdx.graphics.getWidth() / 2f - buttonBackToMenu.getWidth() / 2f) {

            if (buttonBackToMenu.getX() - amount < Gdx.graphics.getWidth() / 2f - buttonBackToMenu.getWidth() / 2f) {
                buttonBackToMenu.setPosition(Gdx.graphics.getWidth() / 2f - buttonBackToMenu.getWidth() / 2f, buttonBackToMenu.getY());
            } else {
                buttonBackToMenu.setPosition(buttonBackToMenu.getX() - amount, buttonBackToMenu.getY());
            }

        } else if (buttonExit.getX() < Gdx.graphics.getWidth() / 2f - buttonExit.getWidth() / 2f) {

            if (buttonExit.getX() + amount > Gdx.graphics.getWidth() / 2f - buttonExit.getWidth() / 2f) {
                buttonExit.setPosition(Gdx.graphics.getWidth() / 2f - buttonExit.getWidth() / 2f, buttonExit.getY());
            } else {
                buttonExit.setPosition(buttonExit.getX() + amount, buttonExit.getY());
            }

        }


    }

    private void gettingBackGame(float delta) {
        float amount = delta * Constants.VELOCITY_PANELS * 3;

        if (buttonResume.getX() + buttonResume.getWidth() > 0) {

            if (buttonResume.getX() + buttonResume.getWidth() - amount < 0) {
                buttonResume.setPosition(-buttonResume.getWidth(), buttonResume.getY());
            } else {
                buttonResume.setPosition(buttonResume.getX() - amount, buttonResume.getY());
            }

        } else if (buttonBackToMenu.getX() < Gdx.graphics.getWidth()) {

            if (buttonBackToMenu.getX() + amount > Gdx.graphics.getWidth()) {
                buttonBackToMenu.setPosition(Gdx.graphics.getWidth(), buttonBackToMenu.getY());
            } else {
                buttonBackToMenu.setPosition(buttonBackToMenu.getX() + amount, buttonBackToMenu.getY());
            }

        } else if (buttonExit.getX() + buttonExit.getWidth() > 0) {

            if (buttonExit.getX() + buttonExit.getWidth() - amount < 0) {
                buttonExit.setPosition(-buttonExit.getWidth(), buttonExit.getY());
            } else {
                buttonExit.setPosition(buttonExit.getX() - amount, buttonExit.getY());
            }

        } else {
            filterBlack.setVisible(false);
            pauseState = PanelState.DOWN;
        }

    }

    private void playerIsLeaving(float delta) {

        float amount = delta * Constants.VELOCITY_PANELS;

        enemyTroll.setSize(enemyTroll.getWidth() + amount, enemyTroll.getHeight() + amount);
        enemyTroll.setOrigin(enemyTroll.getWidth() / 2f, enemyTroll.getHeight() / 2f);
        enemyTroll.setPosition(Gdx.graphics.getWidth() / 2f - enemyTroll.getWidth() / 2f, Gdx.graphics.getHeight() / 2f - enemyTroll.getHeight() / 2f);
        enemyTroll.setRotation(enemyTroll.getRotation() + amount);

        if (enemyTroll.getWidth() > Gdx.graphics.getWidth() * 1.5f && enemyTroll.getRotation() / (360 * 3) > 1) {
            Gdx.app.exit();
        }


    }

    private void playerIsWinning(float delta) {


        if (scoreAnimatedWin < scoreValue) {
            scoreAnimatedWin += delta * MapHelper.instance.getValueThirdStar() / 3;
        } else {
            buttonBackToMenuInGameOver.setVisible(true);
        }

        if (scoreAnimatedWin > MapHelper.instance.getValueFirstStar()) {
            starLeft.setDrawable(textureStarOn);
        }

        if (scoreAnimatedWin > MapHelper.instance.getValueSecondStar()) {
            starCenter.setDrawable(textureStarOn);
        }

        if (scoreAnimatedWin > MapHelper.instance.getValueThirdStar()) {
            starRight.setDrawable(textureStarOn);
        }

        finalScoreText.setText(formatNumber((int) scoreAnimatedWin, Constants.NUMBER_DIGITS_SCORE));


    }


    private void playerIsLosing(float delta) {

        float amount = delta * Constants.VELOCITY_PANELS;

        if (imgGame.getX() < Gdx.graphics.getWidth() / 2f - imgGame.getWidth()) {

            if (imgGame.getX() + amount > Gdx.graphics.getWidth() / 2f - imgGame.getWidth()) {
                imgGame.setX(Gdx.graphics.getWidth() / 2f - imgGame.getWidth());
            } else {
                imgGame.setX(imgGame.getX() + amount);
            }
        } else if (imgOver.getX() > Gdx.graphics.getWidth() / 2f + padding) {

            if (imgOver.getX() - amount < Gdx.graphics.getWidth() / 2f + padding) {
                imgOver.setX(Gdx.graphics.getWidth() / 2f + padding);
            } else {
                imgOver.setX(imgOver.getX() - amount);
            }
        } else {

            gameOverEnemyLeft.setVisible(true);
            gameOverEnemyRight.setVisible(true);
            buttonBackToMenuInGameOver.setVisible(true);

            if (enemiesGameOverState.equals(PanelState.RISE_UP)) {
                gameOverEnemyLeft.setY(gameOverEnemyLeft.getY() + amount);
                gameOverEnemyRight.setY(gameOverEnemyRight.getY() + amount);

                if (gameOverEnemyLeft.getY() > buttonBackToMenuInGameOver.getY() + buttonBackToMenuInGameOver.getHeight() / 2) {
                    enemiesGameOverState = PanelState.RISE_DOWN;
                }
            } else if (enemiesGameOverState.equals(PanelState.RISE_DOWN)) {

                gameOverEnemyLeft.setY(gameOverEnemyLeft.getY() - amount);
                gameOverEnemyRight.setY(gameOverEnemyRight.getY() - amount);

                if (gameOverEnemyLeft.getY() < buttonBackToMenuInGameOver.getY()) {
                    enemiesGameOverState = PanelState.RISE_UP;
                }


            }


        }

    }


    public void activatingModeBuilding() {
        hidingPanelConstruction();
        hidingPanelDetail();
        buttonReverse.setDrawable(buttonCancel);
    }

    public void desactivatingModeBuilding() {
        showingPanelConstruction();
        buttonReverse.setDrawable(buttonHideConstructionPanel);
    }

    public void pausingGame() {
        pauseState = PanelState.RISE_UP;
        filterBlack.setVisible(true);

    }

    public void activatinLoseAnimation() {


        filterBlack.setVisible(true);
        imgGame.setVisible(true);
        imgOver.setVisible(true);

        playerLoseLevel = true;
    }

    public void activatingWinAnimation() {
        filterBlack.setVisible(true);
        starLeft.setVisible(true);
        starCenter.setVisible(true);
        starRight.setVisible(true);
        finalScoreText.setVisible(true);

        scoreAnimatedWin = 0;
        playerWinLevel = true;

    }

    private Image selectRandomEnemy() {

        Image image;
        int option = random.nextInt(3);

        switch (option) {
            case 0:
                image = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_FIRE_ENEMY));
                break;
            case 1:
                image = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_WATER_ENEMY));
                break;
            default:
                image = new Image(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_NORMAL_ENEMY));
                break;
        }

        return image;
    }


    public void resumingGame() {
        pauseState = PanelState.RISE_DOWN;
    }

    public boolean isClickInReverseButton(float x, float y) {
        return x < buttonReverse.getX() + buttonReverse.getWidth() &&
                x > buttonReverse.getX() &&
                y < buttonReverse.getY() + buttonReverse.getHeight() &&
                y > buttonReverse.getY();
    }

    public boolean isClickInPauseButton(float x, float y) {
        return x < buttonPause.getX() + buttonPause.getWidth() &&
                x > buttonPause.getX() &&
                y < buttonPause.getY() + buttonPause.getHeight() &&
                y > buttonPause.getY();
    }

    public boolean isClickInStartRoundButton(float x, float y) {
        return x < buttonStartRound.getX() + buttonStartRound.getWidth() &&
                x > buttonStartRound.getX() &&
                y < buttonStartRound.getY() + buttonStartRound.getHeight() &&
                y > buttonStartRound.getY();
    }

    public boolean isClickInWaterTower(float x, float y) {
        return x < orbWater.getX() + orbWater.getWidth() &&
                x > orbWater.getX() &&
                y < orbWater.getY() + orbWater.getHeight() &&
                y > orbWater.getY();
    }

    public boolean isClickInWindTower(float x, float y) {
        return x < orbWind.getX() + orbWind.getWidth() &&
                x > orbWind.getX() &&
                y < orbWind.getY() + orbWind.getHeight() &&
                y > orbWind.getY();
    }

    public boolean isClickInThunderTower(float x, float y) {
        return x < orbThunder.getX() + orbThunder.getWidth() &&
                x > orbThunder.getX() &&
                y < orbThunder.getY() + orbThunder.getHeight() &&
                y > orbThunder.getY();
    }

    public boolean isClickInFireTower(float x, float y) {
        return x < orbFire.getX() + orbFire.getWidth() &&
                x > orbFire.getX() &&
                y < orbFire.getY() + orbFire.getHeight() &&
                y > orbFire.getY();
    }

    public boolean isClickInTrapdoor(float x, float y) {
        return x < tableTraps.getX() + trapdoor.getX() + trapdoor.getWidth() &&
                x > tableTraps.getX() + trapdoor.getX() &&
                y < tableTraps.getY() + trapdoor.getY() + trapdoor.getHeight() &&
                y > tableTraps.getY() + trapdoor.getY();
    }


    public boolean isClickInConfusionTrap(float x, float y) {
        return x < tableTraps.getX() + trapConfusion.getX() + trapConfusion.getWidth() &&
                x > tableTraps.getX() + trapConfusion.getX() &&
                y < tableTraps.getY() + trapConfusion.getY() + trapConfusion.getHeight() &&
                y > tableTraps.getY() + trapConfusion.getY();
    }

    public boolean isClickInRockTrap(float x, float y) {
        return x < tableTraps.getX() + trapRock.getX() + trapRock.getWidth() &&
                x > tableTraps.getX() + trapRock.getX() &&
                y < tableTraps.getY() + trapRock.getY() + trapRock.getHeight() &&
                y > tableTraps.getY() + trapRock.getY();
    }

    public boolean isClickInButtonBuild(float x, float y) {
        return x < tableDetail.getX() + buttonBuild.getX() + buttonBuild.getWidth() &&
                x > tableDetail.getX() + buttonBuild.getX() &&
                y < tableDetail.getY() + buttonBuild.getY() + buttonBuild.getHeight() &&
                y > tableDetail.getY() + buttonBuild.getY();
    }

    public boolean isClickInButtonEvolve(float x, float y) {
        return x < tableDetail.getX() + buttonEvolve.getX() + buttonEvolve.getWidth() &&
                x > tableDetail.getX() + buttonEvolve.getX() &&
                y < tableDetail.getY() + buttonEvolve.getY() + buttonEvolve.getHeight() &&
                y > tableDetail.getY() + buttonEvolve.getY();
    }

    public boolean isClickInButtonSell(float x, float y) {
        return x < tableDetail.getX() + buttonSell.getX() + buttonSell.getWidth() &&
                x > tableDetail.getX() + buttonSell.getX() &&
                y < tableDetail.getY() + buttonSell.getY() + buttonSell.getHeight() &&
                y > tableDetail.getY() + buttonSell.getY();
    }


    private void resizeFontText(Label text, Table table, float factorWidth, float factorHeight) {
        text.setFontScale((table.getWidth() * factorWidth) / text.getWidth(), (table.getHeight() * factorHeight) / text.getHeight());
        text.setSize(table.getWidth() * factorWidth, table.getHeight() * factorHeight);
    }

    private String formatNumber(int number, int digits) {

        String numberFormated = String.valueOf(number);

        digits -= numberFormated.length();


        while (digits > 0) {
            numberFormated = "0" + numberFormated;
            digits--;
        }

        return numberFormated;

    }


    public void setAmountMoney(int amount) {
        coinValue += amount;
        coinText.setText(formatNumber(coinValue, Constants.NUMBER_DIGITS_MONEY));
    }

    public void diminishHearts(int amount) {
        heartValue -= amount;
        heartText.setText(String.valueOf(heartValue));
        setAmounScore(-MapHelper.instance.getPenaltyHealth());
    }

    public void setAmounScore(int amount) {
        scoreValue += amount;
        scoreText.setText(formatNumber(scoreValue, Constants.NUMBER_DIGITS_SCORE));
    }

    public void setAmountDetailTop(float amount, boolean seconds) {
        detailTop.setText(setMessageDetail(amount, seconds));
    }

    public void setAmountDetailCenter(float amount, boolean seconds) {
        detailCenter.setText(setMessageDetail(amount, seconds));
    }

    public void setAmountDetailBottom(float amount, boolean seconds) {
        detailBottom.setText(setMessageDetail(amount, seconds));
    }

    public void clearDetails() {
        detailTop.setText("");
        detailCenter.setText("");
        detailBottom.setText("");
    }

    public String setMessageDetail(float amount, boolean seconds) {

        String message = "";
        if (amount != 0) {
            message = String.valueOf(amount);

            if (seconds) {
                message += " s";
            }
        }

        return message;


    }

    public boolean isPlayerDead() {
        return heartValue < 1;
    }

    public boolean isPanelConstructionShowed() {
        return panelState.equals(PanelState.UP);
    }

    public boolean isPanelConstructionHide() {
        return panelState.equals(PanelState.DOWN);
    }

    public void showingPanelConstruction() {
        panelState = PanelState.RISE_UP;
    }

    public void hidingPanelConstruction() {
        panelState = PanelState.RISE_DOWN;
    }

    public boolean isPanelDetailShowed() {
        return panelDetailState.equals(PanelState.UP);
    }

    public boolean isPanelDetailHide() {
        return panelDetailState.equals(PanelState.DOWN);
    }

    public void showingPanelDetail() {
        panelDetailState = PanelState.RISE_UP;
    }

    public void hidingPanelDetail() {
        panelDetailState = PanelState.RISE_DOWN;
    }

    public boolean isInPause() {

        return filterBlack.isVisible();
    }

    public int getStars() {

        int stars = 0;

        if (scoreValue > MapHelper.instance.getValueThirdStar()) {
            stars = 3;
        } else if (scoreValue > MapHelper.instance.getValueSecondStar()) {
            stars = 2;
        } else if (scoreValue > MapHelper.instance.getValueFirstStar()) {
            stars = 1;
        }


        return stars;
    }

    //GETTERS


    public Stage getStageGUI() {
        return stageGUI;
    }

    public Image getCoin() {
        return coin;
    }

    public Image getHeart() {
        return heart;
    }

    public Image getOrbFire() {
        return orbFire;
    }

    public Image getOrbWater() {
        return orbWater;
    }

    public Image getOrbWind() {
        return orbWind;
    }

    public Image getOrbThunder() {
        return orbThunder;
    }

    public Image getButtonReverse() {
        return buttonReverse;
    }

    public Label getCoinText() {
        return coinText;
    }

    public Label getScoreText() {
        return scoreText;
    }

    public Label getHeartText() {
        return heartText;
    }

    public int getCoinValue() {
        return coinValue;
    }

    public int getScoreValue() {
        return scoreValue;
    }

    public int getHeartValue() {
        return heartValue;
    }
}
