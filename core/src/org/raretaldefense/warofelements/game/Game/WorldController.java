package org.raretaldefense.warofelements.game.Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Game.GameObjects.AbstractGameObject;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.AbstractTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.FireTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.ThunderTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.WaterTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Towers.WindTower;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.AbstractTrap;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.ConfusionTrap;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.RockTrap;
import org.raretaldefense.warofelements.game.Game.GameObjects.Traps.Trapdoor;
import org.raretaldefense.warofelements.game.Helpers.MapHelper;
import org.raretaldefense.warofelements.game.RaretalDefenseMain;


public class WorldController extends InputAdapter {


    private final String TAG = WorldController.class.getSimpleName();

    public RaretalDefenseMain game;
    public Level level;

    AbstractTower towerSelected;
    AbstractTrap trapSelected;
    TextureRegionDrawable image;
    boolean isReadyForBuild, returnBackToMenu;
    String idLevel;
    Preferences profile;


    public WorldController(RaretalDefenseMain game, String level) {
        this.game = game;
        this.level = new Level(level);
        idLevel = level;
        profile = Gdx.app.getPreferences(Constants.PROFILE_PREFERENCES_SCORES);
        towerSelected = null;
        trapSelected = null;
        image = null;
        isReadyForBuild = false;
        returnBackToMenu = false;
    }


    public void update(float delta) {

        //If the game are returning from pause, then we refresh variables
        if (!level.gui.isInPause()) {
            level.isGamePaused = false;
        } else if (level.gui.playerWantGoMenu) {
            returnBackToMenu = true;
        } else if (level.gui.playerWinLevel) {

            if (profile.getInteger(Constants.PROFILE_STARS_DINAMIC_KEY + idLevel) < level.gui.getStars()) {
                profile.putInteger(Constants.PROFILE_STARS_DINAMIC_KEY + idLevel, level.gui.getStars());
                profile.flush();
            }

        }


        //If the play isn't paused
        if (!level.isGamePaused) {

            //Catching the click
            if (Gdx.input.justTouched()) {
                onClick(Gdx.input.getX(), Gdx.graphics.getHeight() - Gdx.input.getY());
            }
            //Update level
            level.update(delta);

        } else {
            //If we are in pause, we just need update the logic of GUI for update the animations of buttons
            level.gui.update(delta);
        }

    }


    private void onClick(float x, float y) {


        Vector2 cellClicked = MapHelper.instance.positionCell(x, y);
        AbstractGameObject gameObjectClicked = level.mapOfStructures.get(cellClicked);


        //==================================
        //         CAPTURING CLICK
        //==================================

        //Click in button show/hide panel construction
        if (level.gui.isClickInReverseButton(x, y)) {
            isReadyForBuild = false; //We canceling the building if the user has repented
            //We will hide or show the construction panel depending on what the player have done
            if (level.gui.isPanelConstructionShowed()) {
                level.gui.hidingPanelConstruction();
            } else if (level.gui.isPanelConstructionHide()) {
                level.gui.showingPanelConstruction();
            }
            level.userTarget = null;

        } else if (level.gui.isClickInPauseButton(x, y)) {

            level.isGamePaused = true;

            level.gui.pausingGame();


        } else if (level.gui.isClickInStartRoundButton(x, y)) {

            level.gui.startGame();
            level.isRoundStarted = true;


        } else if (level.gui.isPanelConstructionShowed() && level.gui.isClickInFireTower(x, y)) { //Click in Orb Fire
            trapSelected = null;
            level.userTarget = Constants.FIRE_ELEMENT;
            towerSelected = new FireTower(null, new Vector2(0, 0), null, null, null, 0);
        } else if (level.gui.isPanelConstructionShowed() && level.gui.isClickInWaterTower(x, y)) { //Click in Orb Water
            trapSelected = null;
            level.userTarget = Constants.WATER_ELEMENT;
            towerSelected = new WaterTower(null, new Vector2(0, 0), null, null, null, 0);
        } else if (level.gui.isPanelConstructionShowed() && level.gui.isClickInThunderTower(x, y)) { //Click in Orb Thunder
            trapSelected = null;
            level.userTarget = Constants.THUNDER_ELEMENT;
            towerSelected = new ThunderTower(null, new Vector2(0, 0), null, null, null, 0);
        } else if (level.gui.isPanelConstructionShowed() && level.gui.isClickInWindTower(x, y)) { //Click in Orb Wind
            trapSelected = null;
            level.userTarget = Constants.WIND_ELEMENT;
            towerSelected = new WindTower(null, new Vector2(0, 0), null, null, null, 0);
        } else if (level.gui.isPanelConstructionShowed() && level.gui.isClickInRockTrap(x, y)) { //Click in Rock Trap
            towerSelected = null;
            level.userTarget = Constants.ROCK_TRAP;
            trapSelected = new RockTrap(null, new Vector2(0, 0), null, null, null, 0);
        } else if (level.gui.isPanelConstructionShowed() && level.gui.isClickInTrapdoor(x, y)) { //Click in Trapdoor
            towerSelected = null;
            level.userTarget = Constants.TRAPDOOR;
            trapSelected = new Trapdoor(null, new Vector2(0, 0), null, null, null, 0);
        } else if (level.gui.isPanelConstructionShowed() && level.gui.isClickInConfusionTrap(x, y)) { //Click in Confusion Trap
            towerSelected = null;
            level.userTarget = Constants.CONFUSION_TRAP;
            trapSelected = new ConfusionTrap(null, new Vector2(0, 0), null, null, null, 0);
        } else if (level.userTarget != null && !level.userTarget.equals(Constants.EXISTING_STRUCTURE) && level.gui.isClickInButtonBuild(x, y)) { // Click in build button

            //Check if player can build
            if ((towerSelected != null && towerSelected.getCurrentValue() <= level.gui.getCoinValue())
                    || (trapSelected != null && trapSelected.getValue() <= level.gui.getCoinValue())) {
                level.gui.activatingModeBuilding();
                isReadyForBuild = true;
            }

        } else if (level.userTarget != null && level.userTarget.equals(Constants.EXISTING_STRUCTURE) && level.gui.isClickInButtonEvolve(x, y)) { // Click in evolve button

            //Check if player can upgrade
            if (towerSelected != null && towerSelected.canEvolve() && towerSelected.getNextValue() <= level.gui.getCoinValue()) {
                level.gui.setAmountMoney(-towerSelected.getNextValue());
                towerSelected.evolveTower();

            }


        } else if (level.userTarget != null && level.userTarget.equals(Constants.EXISTING_STRUCTURE) && level.gui.isClickInButtonSell(x, y)) { // Click in sell button

            if (towerSelected != null) {

                int moneyToPay = towerSelected.getCurrentValue();


                //Pay to player with a penalitation if the round are started
                if (level.isRoundStarted) {
                    level.gui.setAmountMoney((int) (moneyToPay - towerSelected.getCurrentValue() * Constants.PENALTY_VALUE_SELL));
                } else {
                    level.gui.setAmountMoney(moneyToPay);
                }
                //Unregister structure
                level.mapOfStructures.remove(towerSelected.getPositionTiledMap());
                level.listOfTowersInGame.remove(towerSelected);
            }


            //Clear targets
            level.userTarget = null;

        } else if (level.userTarget != null && isReadyForBuild) { //If the player is building

            if (gameObjectClicked == null) { //Check if exist another structure in the cell position

                Vector2 positionScreen = MapHelper.instance.calculatePixelsPositionCell(cellClicked);

                if (towerSelected != null && !MapHelper.instance.isThroughRoad(cellClicked)) { //Check if the player are building a tower and the cell isn't throughroad

                    //Charge
                    level.gui.setAmountMoney(-towerSelected.getCurrentValue());

                    //Fix variables of map
                    towerSelected.setPositionTiledMap(cellClicked);
                    towerSelected.setPositionScreen(positionScreen);
                    towerSelected.setArea(positionScreen);

                    //Register in lists of level
                    level.mapOfStructures.put(cellClicked, towerSelected);
                    level.listOfTowersInGame.add(towerSelected);

                    //Clear targets
                    level.userTarget = null;
                    isReadyForBuild = false;

                    level.gui.desactivatingModeBuilding();

                } else if (trapSelected != null && MapHelper.instance.isThroughRoad(cellClicked)) { //Otherwise, check if the player are positioning the trap in the correct place.
                    //Charge
                    level.gui.setAmountMoney(-trapSelected.getValue());

                    //Fix variables of map
                    trapSelected.setPositionTiledMap(cellClicked);
                    trapSelected.setPositionScreen(positionScreen);

                    //Register in lists of level
                    level.mapOfStructures.put(cellClicked, trapSelected);
                    level.listOfTrapsInGame.add(trapSelected);

                    //Clear targets
                    level.userTarget = null;
                    isReadyForBuild = false;

                    level.gui.desactivatingModeBuilding();
                }


            } else {
                //The player are building in a existing structure
                //TODO mensaje de error o do nothing?
            }


        } else { //If the place on the player are clicked is none of the above, then it means that the player are touching the map


            if (isReadyForBuild) {
                level.gui.desactivatingModeBuilding();
            }


            //Clear targets
            level.userTarget = null;
            towerSelected = null;
            trapSelected = null;
            isReadyForBuild = false;


            //If exist another structure, then we catch it to show later
            if (gameObjectClicked != null) {
                level.userTarget = Constants.EXISTING_STRUCTURE;
                if (gameObjectClicked instanceof AbstractTower) {
                    towerSelected = (AbstractTower) gameObjectClicked;
                } else if (gameObjectClicked instanceof AbstractTrap) {
                    trapSelected = (AbstractTrap) gameObjectClicked;
                }
            }


        }


        //==================================
        //         SHOWING INFO
        //==================================

        //Next, go to show info if there are
        if (level.userTarget != null && !isReadyForBuild) {
            if (towerSelected != null) {
                //Catch many data
                image = new TextureRegionDrawable(towerSelected.getImagesOfCanons().get(towerSelected.getLevel()));
                boolean isNew = !level.userTarget.equals(Constants.EXISTING_STRUCTURE);
                int price = 0;


                //If the structure already exists we need calculate the price from other way
                if (isNew) {
                    price = towerSelected.getCurrentValue();
                } else if (towerSelected.canEvolve()) {
                    price = towerSelected.getNextValue();
                }

                //Send it properly
                if (towerSelected instanceof WindTower) {
                    level.gui.updateDetail(isNew, Constants.TYPE_DETAIL_TOWER_WIND, image, price, towerSelected.getCurrentRange(), towerSelected.getCurrentVelocityAtack());
                } else {
                    level.gui.updateDetail(isNew, Constants.TYPE_DETAIL_DEFAULT_TOWER, image, price, towerSelected.getCurrentDamage(), towerSelected.getCurrentRange(), towerSelected.getCurrentVelocityAtack());
                }
            } else if (trapSelected != null) { //The same way but with traps (just change the data that we are sending)
                image = new TextureRegionDrawable(trapSelected.getImage());
                boolean isNew = !level.userTarget.equals(Constants.EXISTING_STRUCTURE);

                if (isNew) {
                    if (trapSelected instanceof RockTrap) {
                        level.gui.updateDetail(isNew, Constants.TYPE_DETAIL_ROCK_TRAP, image, trapSelected.getValue(), trapSelected.getDamage(), trapSelected.getCooldown());
                    } else if (trapSelected instanceof Trapdoor) {
                        level.gui.updateDetail(isNew, Constants.TYPE_DETAIL_TRAPDOOR, image, trapSelected.getValue(), trapSelected.getCooldown());
                    } else if (trapSelected instanceof ConfusionTrap) {
                        level.gui.updateDetail(isNew, Constants.TYPE_DETAIL_CONFUSION_TRAP, image, trapSelected.getValue(), trapSelected.getCooldown(), trapSelected.getCooldownActiveTrap());
                    }
                } else {
                    if (trapSelected.isItCanBeUsed()) {
                        trapSelected.setItCanBeUsed(false);
                        trapSelected.setTheActivationPast(false);
                    }
                }

            }

        } else { //But if we don't go to show info to player, we need be safe and hide the panel of details

            if (level.gui.isPanelDetailShowed()) {
                level.gui.hidingPanelDetail();
            }

        }

    }

    public boolean gettingBackToMenu() {
        return returnBackToMenu;
    }

}

