package org.raretaldefense.warofelements.game.Game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.MapHelper;


public class WorldRenderer implements Disposable {

    private OrthographicCamera cameraGame, cameraTiled;
    private SpriteBatch batch;
    private WorldController worldController;
    private TiledMap tiledMap;
    private TiledMapRenderer tiledMapRenderer;


    public WorldRenderer(WorldController worldController) {
        this.worldController = worldController;
        init();
    }

    public void init() {

        Constants.SIZE_CELL_WIDTH = (int) (Gdx.graphics.getWidth() / MapHelper.instance.getNumberCellsWidth());
        Constants.SIZE_CELL_HEIGHT = (int) (Gdx.graphics.getHeight() / MapHelper.instance.getNumberCellsHeight());


        //Sprite Batch
        this.batch = worldController.game.batch;

        //Map
        tiledMap = worldController.level.tiledMap;

        //RenderMap
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, Constants.unitScale);

        Vector2 dimensionsTiledMap = MapHelper.instance.getPixelsDimensions();

        //Game cameraGame
        cameraGame = new OrthographicCamera(MapHelper.instance.getNumberCellsWidth() * Constants.SIZE_CELL_WIDTH,
                MapHelper.instance.getNumberCellsHeight() * Constants.SIZE_CELL_HEIGHT);
        cameraGame.setToOrtho(false);
        // cameraGame.viewportWidth = MapHelper.instance.getNumberCellsWidth()* Constants.SIZE_CELL_WIDTH;
        // cameraGame.viewportHeight = MapHelper.instance.getNumberCellsHeight() * Constants.SIZE_CELL_HEIGHT;


        cameraGame.update();

        //Tiled cameraTiled
        cameraTiled = new OrthographicCamera();
        cameraTiled.setToOrtho(false, MapHelper.instance.getNumberCellsWidth(), MapHelper.instance.getNumberCellsHeight());
        //cameraTiled.position.set(dimensionsTiledMap.x / 2, dimensionsTiledMap.y / 2, 0);
        cameraTiled.update();


    }


    public void render() {


        tiledMapRenderer.setView(cameraTiled);
        tiledMapRenderer.render();
        batch.setProjectionMatrix(cameraGame.combined);
        worldController.level.render(batch);


    }

    @Override
    public void dispose() {

    }

    public void resize(float width, float height) {

        //TODO hace falta?
        // cameraGui.viewportWidth = width;

    }


}
