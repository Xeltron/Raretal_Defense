package org.raretaldefense.warofelements.game.Helpers;


import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

import org.raretaldefense.warofelements.game.Constants.Constants;


public class Assets implements Disposable, AssetErrorListener {


    public static final Assets instance = new Assets();

    private static final String TAG = Assets.class.getSimpleName();

    public AssetManager assetManager;

    public AssetsFonts fonts;

    // Singleton: Prevent instantiation from other classes
    private Assets() {
    }

    public void init(AssetManager assetManager) {
        this.assetManager = assetManager;

        // set assetManager error handler
        assetManager.setErrorListener(instance);
        // load Texture Atlas
        if (Gdx.app.getType().equals(Application.ApplicationType.Desktop)) {
            assetManager.load(Gdx.files.absolute(Constants.TEXTURE_ATLAS_OBJECTS_DESKTOP).path(), TextureAtlas.class);
            assetManager.load(Gdx.files.absolute(Constants.TEXTURE_ATLAS_MENU_OBJECTS_DESKTOP).path(), TextureAtlas.class);

        } else {
            assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);
            assetManager.load(Constants.TEXTURE_ATLAS_MENU_OBJECTS, TextureAtlas.class);
        }
        // start loading instance and wait until finished
        assetManager.finishLoading();


        Gdx.app.debug(TAG, "# de instance cargados: "
                + assetManager.getAssetNames().size);

        for (String a : assetManager.getAssetNames())
            Gdx.app.debug(TAG, "asset: " + a);


        fonts = new AssetsFonts();

    }


    public static void reload() {

    }


    public TextureAtlas getTextureAtlasMenu() {
        if (Gdx.app.getType().equals(Application.ApplicationType.Desktop)) {
            return assetManager.get(Gdx.files.absolute(Constants.TEXTURE_ATLAS_MENU_OBJECTS_DESKTOP).path());
        } else {
            return assetManager.get(Constants.TEXTURE_ATLAS_MENU_OBJECTS);
        }

    }

    public TextureAtlas getTextureAtlasGame() {
        if (Gdx.app.getType().equals(Application.ApplicationType.Desktop)) {
            return assetManager.get(Gdx.files.absolute(Constants.TEXTURE_ATLAS_OBJECTS_DESKTOP).path());
        } else {
            return assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);
        }

    }


    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset '" +
                asset.fileName + "'", throwable);
    }

    @Override
    public void dispose() {

        assetManager.dispose();
        fonts.dispose();
    }

    public Vector2 dimensionsOfGameObject(Vector2 dimensionsTexture) {
        return dimensionsOfGameObject(dimensionsTexture.x, dimensionsTexture.y);
    }


    public Vector2 dimensionsOfGameObject(float widthTexture, float heightTexture) {

        float widthGameObject = widthTexture * Constants.SIZE_CELL_WIDTH / 100;
        float heightGameObject = heightTexture * Constants.SIZE_CELL_HEIGHT / 100;

        return new Vector2(widthGameObject, heightGameObject);

    }


}
