package org.raretaldefense.warofelements.game.Helpers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.Disposable;

import org.raretaldefense.warofelements.game.Constants.Constants;


public class AssetsFonts implements Disposable {

    public final BitmapFont fontLittle, fontMedium, fontBig;


    public AssetsFonts() {

        if (Gdx.app.getType().equals(Application.ApplicationType.Desktop)) {
            fontLittle = new BitmapFont(Gdx.files.absolute(Constants.PATH_FONTS_DESKTOP + "/" + Constants.FONT_CALADEA_LITTLE));
            fontMedium = new BitmapFont(Gdx.files.absolute(Constants.PATH_FONTS_DESKTOP + "/" + Constants.FONT_CALADEA_MEDIUM));
            fontBig = new BitmapFont(Gdx.files.absolute(Constants.PATH_FONTS_DESKTOP + "/" + Constants.FONT_CALADEA_BIG));
        } else {
            fontLittle = new BitmapFont(Gdx.files.internal(Constants.PATH_FONTS + "/" + Constants.FONT_CALADEA_LITTLE));
            fontMedium = new BitmapFont(Gdx.files.internal(Constants.PATH_FONTS + "/" + Constants.FONT_CALADEA_MEDIUM));
            fontBig = new BitmapFont(Gdx.files.internal(Constants.PATH_FONTS + "/" + Constants.FONT_CALADEA_BIG));

        }


        fontLittle.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        fontMedium.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        fontBig.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

    }

    @Override
    public void dispose() {
        fontLittle.dispose();
        fontMedium.dispose();
        fontBig.dispose();
    }
}
