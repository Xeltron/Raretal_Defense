package org.raretaldefense.warofelements.game.Helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

import org.raretaldefense.warofelements.game.Constants.Constants;

import java.util.HashMap;
import java.util.Map;

import static com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;

public class MapHelper {

    private final String TAG = getClass().getSimpleName();
    private TiledMap map;
    private MapProperties mapProperties;
    public Map<Vector2, Cell> mapCells;
    public Vector2 cellRespawn, cellExit;
    private OrthographicCamera cameraTiled;

    public final static MapHelper instance = new MapHelper();

    public void init(TiledMap tiledMap) {
        map = tiledMap;
        mapProperties = tiledMap.getProperties();
        mapCells = new HashMap<Vector2, Cell>();
        cellRespawn = null;
        cellExit = null;
    }

    private MapHelper() {
    }

    public int getNumberRounds() {

        return Integer.parseInt((String) mapProperties.get(Constants.TILEDMAP_ROUNDS));
    }

    public int getStarterCoins() {
        return Integer.parseInt((String) mapProperties.get(Constants.TILEDMAP_FIRST_COINS));
    }

    public int getStarterEnemies() {
        return Integer.parseInt((String) mapProperties.get(Constants.TILEDMAP_FIRST_ENEMIES));
    }


    public int getPenaltyHealth() {
        return Integer.parseInt((String) mapProperties.get(Constants.TILEDMAP_PENALTY_HEALTH));
    }

    public int getValueFirstStar() {
        return Integer.parseInt((String) mapProperties.get(Constants.TILEDMAP_STAR_FIRST));
    }

    public int getValueSecondStar() {
        return Integer.parseInt((String) mapProperties.get(Constants.TILEDMAP_STAR_SECOND));
    }

    public int getValueThirdStar() {
        return Integer.parseInt((String) mapProperties.get(Constants.TILEDMAP_STAR_THIRD));
    }

    public float getNumberCellsWidth() {
        return Float.valueOf((Integer) mapProperties.get(Constants.TILEDMAP_WIDTH));
    }

    public float getNumberCellsHeight() {
        return Float.valueOf((Integer) mapProperties.get(Constants.TILEDMAP_HEIGHT));
    }

    public float getWidthCell() {
        return Float.valueOf((Integer) mapProperties.get(Constants.TILEDMAP_TILEWIDTH));
    }


    public float getHeightCell() {
        return Float.valueOf((Integer) mapProperties.get(Constants.TILEDMAP_TILEHEIGHT));
    }


    public Vector2 getPixelsDimensions() {
        return new Vector2(
                getNumberCellsWidth() * getWidthCell(),
                getNumberCellsHeight() * getHeightCell());
    }


    public int positionCellX(float positionPixelX) {
        return (int) (getNumberCellsWidth() * positionPixelX / Gdx.graphics.getWidth());
    }

    public int positionCellY(float positionPixelY) {
        return (int) (getNumberCellsHeight() * positionPixelY / Gdx.graphics.getHeight());
    }

    public Vector2 positionCell(float positionPixelX, float positionPixelY) {
        return new Vector2(positionCellX(positionPixelX), positionCellY(positionPixelY));
    }

    private Cell getCell(String layerName, int positionCellX, int positionCellY) {


        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(layerName);

        if (layer == null) {
            Gdx.app.error(TAG, "El nombre de la capa es incorrecto");
            return null;
        }

        Cell cell = layer.getCell(positionCellX, positionCellY);

        if (cell == null) return null;

        return cell;

    }

    public Cell getTerrainCell(int positionCellX, int positionCellY) {
        return getCell(Constants.TILEDMAP_TERRAIN_LAYER, positionCellX, positionCellY);
    }

    public Cell getTerrainCell(Vector2 position) {
        return getTerrainCell((int) position.x, (int) position.y);
    }


    public MapProperties getCellProperties(String layerName, int positionCellX, int positionCellY) {

        Cell cell = getCell(layerName, positionCellX, positionCellY);
        if (cell == null) {
            return null;
        }
        return cell.getTile().getProperties();
    }


    public MapProperties getTerrainCellProperties(int positionCellX, int positionCellY) {
        return getCellProperties(Constants.TILEDMAP_TERRAIN_LAYER, positionCellX, positionCellY);
    }

    public MapProperties getConfigCellProperties(int positionCellX, int positionCellY) {
        return getCellProperties(Constants.TILEDMAP_RESPAWN_EXIT_LAYER, positionCellX, positionCellY);
    }

    public boolean isThroughRoad(int positionCellX, int positionCellY) {
        return "true".equals(getTerrainCellProperties(positionCellX, positionCellY).get(Constants.TILEDMAP_ATTRIBUTE_THROUGH_ROAD));
    }

    public boolean isThroughRoad(Vector2 positionCell) {
        return isThroughRoad((int) positionCell.x, (int) positionCell.y);
    }

    public boolean isCorner(int positionCellX, int positionCellY) {

        //If the cell contains the attribute "up" this mean that the cell is a corner. We don't need check all the attributes left over.
        return getTerrainCellProperties(positionCellX, positionCellY).get(Constants.TILEDMAP_ATTRIBUTE_UP) != null;
    }

    public boolean canUp(int positionCellX, int positionCellY) {
        if (isCorner(positionCellX, positionCellY)) {

            Object attributeUp = getTerrainCellProperties(positionCellX, positionCellY).get(Constants.TILEDMAP_ATTRIBUTE_UP);

            if (attributeUp != null)
                return "true".equals(attributeUp);
        }
        return false;
    }

    public boolean canDown(int positionCellX, int positionCellY) {
        if (isCorner(positionCellX, positionCellY)) {

            Object attributeDown = getTerrainCellProperties(positionCellX, positionCellY).get(Constants.TILEDMAP_ATTRIBUTE_DOWN);

            if (attributeDown != null)
                return "true".equals(attributeDown);
        }
        return false;
    }

    public boolean canLeft(int positionCellX, int positionCellY) {
        if (isCorner(positionCellX, positionCellY)) {

            Object attributeLeft = getTerrainCellProperties(positionCellX, positionCellY).get(Constants.TILEDMAP_ATTRIBUTE_LEFT);

            if (attributeLeft != null)
                return "true".equals(attributeLeft);
        }
        return false;
    }

    public boolean canRight(int positionCellX, int positionCellY) {
        if (isCorner(positionCellX, positionCellY)) {

            Object attributeRight = getTerrainCellProperties(positionCellX, positionCellY).get(Constants.TILEDMAP_ATTRIBUTE_RIGHT);

            if (attributeRight != null)
                return "true".equals(attributeRight);
        }
        return false;
    }


    public boolean canUp(Cell currentCell) {
        String attribute = (String) currentCell.getTile().getProperties().get(Constants.TILEDMAP_ATTRIBUTE_UP);
        return attribute != null && attribute.equals("true");
    }

    public boolean canDown(Cell currentCell) {
        String attribute = (String) currentCell.getTile().getProperties().get(Constants.TILEDMAP_ATTRIBUTE_DOWN);
        return attribute != null && attribute.equals("true");
    }

    public boolean canLeft(Cell currentCell) {
        String attribute = (String) currentCell.getTile().getProperties().get(Constants.TILEDMAP_ATTRIBUTE_LEFT);
        return attribute != null && attribute.equals("true");
    }

    public boolean canRight(Cell currentCell) {
        String attribute = (String) currentCell.getTile().getProperties().get(Constants.TILEDMAP_ATTRIBUTE_RIGHT);
        return attribute != null && attribute.equals("true");
    }

    public boolean isRespawn(int positionCellX, int positionCellY) {

        MapProperties cellProperties = getConfigCellProperties(positionCellX, positionCellY);

        if (cellProperties == null) return false;

        Object attributeRespawn = cellProperties.get(Constants.TILEDMAP_ATTRIBUTE_RESPAWN);

        if (attributeRespawn != null) return "true".equals(attributeRespawn);
        else return false;
    }

    public boolean isExit(int positionCellX, int positionCellY) {


        MapProperties cellProperties = getConfigCellProperties(positionCellX, positionCellY);

        if (cellProperties == null) return false;

        Object attributeExit = cellProperties.get(Constants.TILEDMAP_ATTRIBUTE_EXIT);


        return attributeExit != null && "true".equals(attributeExit);

    }

    public void readMap() {

        for (int column = 0; column < MapHelper.instance.getNumberCellsWidth(); column++) {

            for (int row = 0; row < MapHelper.instance.getNumberCellsHeight(); row++) {

                //If cellRespawn o cellCorner is unnasigned...
                if (cellRespawn == null || cellExit == null) {
                    //And the current cell is in the borders...
                    if (column == 0 || column == MapHelper.instance.getNumberCellsWidth() - 1 ||
                            row == 0 || row == MapHelper.instance.getNumberCellsHeight() - 1) {

                        /*
                            And this cell have the property respawn, then we add it to his variable.
                            Or if it have the property exit, we catch it.
                         */
                        if (cellRespawn == null && MapHelper.instance.isRespawn(column, row)) {
                            cellRespawn = new Vector2(column, row);
                            continue;
                        } else if (MapHelper.instance.isExit(column, row)) {
                            cellExit = new Vector2(column, row);
                            continue;
                        }
                    }
                }

                /*
                    We catch the corners of ThroughRoad for more efficient.

                    Each enemy need be updated, if each enemy access to properties of map
                    every time, we will lose resources that can be necessary to calculate all
                    the logic and render all the sprites.

                    In this way, we will accessed in properties just when the enemy enter in
                    cell that it is a corner for the later processing of logic.
                 */
                if (MapHelper.instance.isCorner(column, row)) {
                    //   cellCorners.add(new Vector2(column, row));
                    Vector2 positionCell = new Vector2(column, row);
                    mapCells.put(positionCell, getTerrainCell(positionCell));
                }

            }

        }


    }

    public Vector2 calculatePixelsPositionCell(Vector2 coordenatesCell) {


        float x = coordenatesCell.x * Gdx.graphics.getWidth() / getNumberCellsWidth();
        float y = coordenatesCell.y * Gdx.graphics.getHeight() / getNumberCellsHeight();

        return new Vector2(x, y);


    }

    public float getWidthCellInPixels() {
        return Gdx.graphics.getWidth() / getNumberCellsWidth();
    }

    public float getHeightCellInPixels() {
        return Gdx.graphics.getHeight() / getNumberCellsHeight();
    }

    public OrthographicCamera getCameraTiled() {
        return cameraTiled;
    }

    public void setCameraTiled(OrthographicCamera cameraTiled) {
        this.cameraTiled = cameraTiled;
    }
}