package org.raretaldefense.warofelements.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import org.raretaldefense.warofelements.game.Helpers.Assets;
import org.raretaldefense.warofelements.game.Screens.MenuScreen;

public class RaretalDefenseMain extends Game {


    private final String TAG = getClass().getSimpleName();

    public SpriteBatch batch;

    @Override
    public void create() {

        Gdx.app.setLogLevel(Application.LOG_DEBUG);


        Gdx.app.debug(TAG, "Inicializamos el ciclo de vida del juego");

        Assets.instance.init(new AssetManager());

        batch = new SpriteBatch();


        // The game inizialize the MenuScreen
        setScreen(new MenuScreen(this));


    }


    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);


        super.render();


    }

    @Override
    public void dispose() {


        Gdx.app.debug(TAG, "Finalizamos el ciclo de vida del juego");

        batch.dispose();
        super.dispose();
    }


}
