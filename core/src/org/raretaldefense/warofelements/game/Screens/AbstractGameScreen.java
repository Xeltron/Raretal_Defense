package org.raretaldefense.warofelements.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

import org.raretaldefense.warofelements.game.RaretalDefenseMain;

/**
 * Created by iam47189294 on 20/04/16.
 */
public abstract class AbstractGameScreen implements Screen {

    private final String TAG = getClass().getName();

    RaretalDefenseMain game;

    public AbstractGameScreen(Game game) {
        this.game = (RaretalDefenseMain) game;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
