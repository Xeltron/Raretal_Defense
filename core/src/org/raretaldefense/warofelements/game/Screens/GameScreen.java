package org.raretaldefense.warofelements.game.Screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;

import org.raretaldefense.warofelements.game.Game.WorldController;
import org.raretaldefense.warofelements.game.Game.WorldRenderer;


public class GameScreen extends AbstractGameScreen {

    private final String TAG = getClass().getSimpleName();


    WorldController worldController;
    WorldRenderer vista;
    String level;


    public GameScreen(Game game, String level) {
        super(game);
        this.level = level;
    }

    @Override
    public void show() {

        Gdx.app.debug(TAG, "Mostramos la pantalla W:" + Gdx.graphics.getWidth() + " H:" + Gdx.graphics.getHeight());

        worldController = new WorldController(game, level);
        vista = new WorldRenderer(worldController);
    }

    @Override
    public void render(float delta) {

        if (Gdx.app.getType().equals(Application.ApplicationType.Android)) delta *= 2;

        worldController.update(delta);

        //If the user are exit of game
        gettinBackToMenu();

        vista.render();

    }


    private void gettinBackToMenu() {

        if (worldController.gettingBackToMenu()) {
            game.setScreen(new MenuScreen(game));
        }


    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resize(int width, int height) {

        super.resize(width, height);
        vista.resize(width, height);
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void hide() {
        Gdx.app.debug(TAG, "Ocultamos la pantalla");
    }
}
