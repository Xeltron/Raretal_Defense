package org.raretaldefense.warofelements.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;


public class LevelSelectScreen extends AbstractGameScreen {
    private final String TAG = getClass().getSimpleName();

    private TextureRegionDrawable starOn, starOff;
    private Array<Image> starsLevel1, starsLevel2, starsLevel3;
    private ImageButton buttonBack;

    private Stage stage;

    Preferences profile;

    public LevelSelectScreen(Game game) {
        super(game);
    }


    @Override
    public void show() {


        profile = Gdx.app.getPreferences(Constants.PROFILE_PREFERENCES_SCORES);


        stage = new Stage();

        // Enable input system
        Gdx.input.setInputProcessor(stage);


        //==================================
        //      INITIALIZING VARIABLES
        //==================================

        float padding = Gdx.graphics.getWidth() / 100f;

        starsLevel1 = new Array<Image>(Constants.SELECT_LEVEL_NUMBER_STARS);
        starsLevel2 = new Array<Image>(Constants.SELECT_LEVEL_NUMBER_STARS);
        starsLevel3 = new Array<Image>(Constants.SELECT_LEVEL_NUMBER_STARS);


        // Style and Textures
        TextureRegionDrawable level1Up = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.SELECT_LEVEL_BUTTON_01_UP));
        TextureRegionDrawable level1Down = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.SELECT_LEVEL_BUTTON_01_DOWN));
        TextureRegionDrawable level2Up = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.SELECT_LEVEL_BUTTON_02_UP));
        TextureRegionDrawable level2Down = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.SELECT_LEVEL_BUTTON_02_DOWN));
        TextureRegionDrawable level3Up = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.SELECT_LEVEL_BUTTON_03_UP));
        TextureRegionDrawable level3Down = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.SELECT_LEVEL_BUTTON_03_DOWN));
        starOn = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.REFERENCE_NAME_ATLAS_STAR_ON));
        starOff = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.REFERENCE_NAME_ATLAS_STAR_OFF));
        Image imageLevel1 = new Image(Assets.instance.getTextureAtlasMenu().findRegions(Constants.SELECT_LEVEL_MAP).get(0));
        Image imageLevel2 = new Image(Assets.instance.getTextureAtlasMenu().findRegions(Constants.SELECT_LEVEL_MAP).get(1));
        Image imageLevel3 = new Image(Assets.instance.getTextureAtlasMenu().findRegions(Constants.SELECT_LEVEL_MAP).get(2));


        // Widgets
        Image background = new Image(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BACKGROUND));
        ImageButton buttonLevel1 = new ImageButton(level1Up, level1Down);
        ImageButton buttonLevel2 = new ImageButton(level2Up, level2Down);
        ImageButton buttonLevel3 = new ImageButton(level3Up, level3Down);
        buttonBack = new ImageButton(new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.TUTORIAL_BACK)));

        for (int i = 0; i < Constants.SELECT_LEVEL_NUMBER_STARS; i++) {
            starsLevel1.add(new Image());
            starsLevel2.add(new Image());
            starsLevel3.add(new Image());
        }


        //==================================
        //  CONFIGURING TABLES AND WIDGETS
        //==================================


        //Background
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        //Button back
        buttonBack.setSize(Gdx.graphics.getWidth() / 5f, Gdx.graphics.getHeight() / 5f);
        buttonBack.setPosition(Gdx.graphics.getWidth() - buttonBack.getWidth() - padding * 2, padding * 2);
        buttonBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
                super.clicked(event, x, y);
            }
        });

        //Level 1
        imageLevel1.setSize(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 3);
        imageLevel1.setPosition(padding * 2, Gdx.graphics.getHeight() - imageLevel1.getHeight() - padding * 2);

        buttonLevel1.setSize(imageLevel1.getWidth() / 3, imageLevel1.getHeight() / 3);
        buttonLevel1.setPosition(imageLevel1.getX() + imageLevel1.getWidth() / 2 - buttonLevel1.getWidth() / 2, imageLevel1.getY() + imageLevel1.getHeight() / 5);
        buttonLevel1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game, "01"));
                super.clicked(event, x, y);
            }
        });


        asignNumberOfStars(profile.getInteger(Constants.PROFILE_STARS_LEVEL01, 0), starsLevel1, imageLevel1);


        //Level 2
        imageLevel2.setSize(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 3);
        imageLevel2.setPosition(Gdx.graphics.getWidth() / 2 - imageLevel2.getWidth() / 2, Gdx.graphics.getHeight() - imageLevel2.getHeight() - padding * 2);

        buttonLevel2.setSize(imageLevel2.getWidth() / 3, imageLevel2.getHeight() / 3);
        buttonLevel2.setPosition(imageLevel2.getX() + imageLevel2.getWidth() / 2 - buttonLevel2.getWidth() / 2, imageLevel2.getY() + imageLevel2.getHeight() / 5);
        buttonLevel2.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game, "02"));
                super.clicked(event, x, y);
            }
        });

        asignNumberOfStars(profile.getInteger(Constants.PROFILE_STARS_LEVEL02, 0), starsLevel2, imageLevel2);

        //Level 3
        imageLevel3.setSize(Gdx.graphics.getWidth() / 4, Gdx.graphics.getHeight() / 3);
        imageLevel3.setPosition(Gdx.graphics.getWidth() - imageLevel3.getWidth() - padding * 2, Gdx.graphics.getHeight() - imageLevel3.getHeight() - padding * 2);

        buttonLevel3.setSize(imageLevel3.getWidth() / 3, imageLevel3.getHeight() / 3);
        buttonLevel3.setPosition(imageLevel3.getX() + imageLevel3.getWidth() / 2 - buttonLevel3.getWidth() / 2, imageLevel3.getY() + imageLevel3.getHeight() / 5);
        buttonLevel3.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new GameScreen(game, "03"));
                super.clicked(event, x, y);
            }
        });

        asignNumberOfStars(profile.getInteger(Constants.PROFILE_STARS_LEVEL03, 0), starsLevel3, imageLevel3);

        //==================================
        //         ADDING ACTORS
        //==================================


        //Add all elements to stage in priority order
        stage.addActor(background);
        stage.addActor(imageLevel1);
        stage.addActor(imageLevel2);
        stage.addActor(imageLevel3);
        stage.addActor(buttonLevel1);
        stage.addActor(buttonLevel2);
        stage.addActor(buttonLevel3);
        stage.addActor(buttonBack);

        for (int i = 0; i < Constants.SELECT_LEVEL_NUMBER_STARS; i++) {
            stage.addActor(starsLevel1.get(i));
            stage.addActor(starsLevel2.get(i));
            stage.addActor(starsLevel3.get(i));
        }


    }


    @Override
    public void hide() {
        Gdx.app.debug(TAG, "Ocultamos la pantalla");
    }

    @Override
    public void render(float delta) {

        update();

        stage.draw();

    }


    private void update() {

    }


    public void asignNumberOfStars(int numberOfStars, Array<Image> images, Image mapOfLevel) {

        for (int i = 0; i < Constants.SELECT_LEVEL_NUMBER_STARS; i++) {

            if (i < numberOfStars) {
                images.get(i).setDrawable(starOn);
            } else {
                images.get(i).setDrawable(starOff);
            }

            images.get(i).setSize(mapOfLevel.getWidth() / Constants.SELECT_LEVEL_NUMBER_STARS, mapOfLevel.getHeight() / 3);
            images.get(i).setPosition(mapOfLevel.getX() + images.get(i).getWidth() * i, mapOfLevel.getY() - images.get(i).getHeight());


        }

    }

}

