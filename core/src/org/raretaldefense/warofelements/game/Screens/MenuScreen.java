package org.raretaldefense.warofelements.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;

public class MenuScreen extends AbstractGameScreen {


    private final String TAG = getClass().getSimpleName();
    private Stage stage;

    public MenuScreen(Game game) {
        super(game);
    }


    @Override
    public void show() {

        Gdx.app.debug(TAG, "Mostramos la pantalla");
        stage = new Stage();

        // Enable input system
        Gdx.input.setInputProcessor(stage);

        //==================================
        //      INITIALIZING VARIABLES
        //==================================

        //Padding for better positioning
        float padding = Gdx.graphics.getWidth() / 100f;

        //Textures
        TextureRegionDrawable texturePanelButtons = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_PANEL_BUTTONS));
        TextureRegionDrawable texturePlayUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_PLAY_UP));
        TextureRegionDrawable texturePlayDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_PLAY_DOWN));
        TextureRegionDrawable textureTutorialUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_TUTORIAL_UP));
        TextureRegionDrawable textureTutorialDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_TUTORIAL_DOWN));
        TextureRegionDrawable textureOptionsUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_OPTIONS_UP));
        TextureRegionDrawable textureOptionsDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_OPTIONS_DOWN));
        TextureRegionDrawable textureExitUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_EXIT_UP));
        TextureRegionDrawable textureExitDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BUTTON_EXIT_DOWN));

        // Widgets
        Image background = new Image(Assets.instance.getTextureAtlasMenu().findRegion(Constants.MENU_BACKGROUND));
        ImageButton buttonPlay = new ImageButton(texturePlayUp, texturePlayDown);
        ImageButton buttonTutorial = new ImageButton(textureTutorialUp, textureTutorialDown);
        ImageButton buttonOptions = new ImageButton(textureOptionsUp, textureOptionsDown);
        ImageButton buttonExit = new ImageButton(textureExitUp, textureExitDown);


        //==================================
        //  CONFIGURING TABLES AND WIDGETS
        //==================================

        //Background
        background.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        // Table details
        Table table = new Table();
        table.setSize(Gdx.graphics.getWidth() / 3f, Gdx.graphics.getHeight());
        table.setBackground(texturePanelButtons);
        table.setPosition(Gdx.graphics.getWidth() - table.getWidth(), 0);

        //Button play
        buttonPlay.setSize(table.getWidth() / 2f, table.getHeight() / 7f);
        buttonPlay.setPosition(table.getWidth() / 5f, table.getHeight() / 2f + buttonPlay.getHeight() - padding * 5);
        buttonPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new LevelSelectScreen(game));
                super.clicked(event, x, y);
            }
        });

        //Button Tutorial
        buttonTutorial.setSize(table.getWidth() / 2f, table.getHeight() / 7f);
        buttonTutorial.setPosition(table.getWidth() / 5f, table.getHeight() / 2f - padding * 5);
        buttonTutorial.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new TutorialScreen(game));
                super.clicked(event, x, y);
            }
        });

        //Button Options
        buttonOptions.setSize(table.getWidth() / 2f, table.getHeight() / 7f);
        buttonOptions.setPosition(table.getWidth() / 5f, table.getHeight() / 2f - buttonOptions.getHeight() - padding * 5);
        buttonOptions.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                // game.setScreen(new ConfigurationScreen(game));
                super.clicked(event, x, y);
            }
        });

        //Button Exit
        buttonExit.setSize(table.getWidth() / 2f, table.getHeight() / 7f);
        buttonExit.setPosition(table.getWidth() / 5f, table.getHeight() / 2f - buttonExit.getHeight() - buttonOptions.getHeight() - padding * 5);
        buttonExit.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
                super.clicked(event, x, y);
            }
        });


        //==================================
        //         ADDING ACTORS
        //==================================

        // Table construction
        table.addActor(buttonPlay);
        table.addActor(buttonTutorial);
        table.addActor(buttonOptions);
        table.addActor(buttonExit);

        // Adding table into stage
        stage.addActor(background);
        stage.addActor(table);


    }


    @Override
    public void hide() {
        Gdx.app.debug(TAG, "Ocultamos la pantalla");
    }

    @Override
    public void render(float delta) {

        update();

        stage.draw();
    }


    private void update() {

    }

}

