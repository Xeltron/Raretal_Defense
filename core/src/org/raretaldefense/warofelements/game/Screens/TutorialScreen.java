package org.raretaldefense.warofelements.game.Screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.Helpers.Assets;


public class TutorialScreen extends AbstractGameScreen {


    private final String TAG = getClass().getSimpleName();
    private Stage stage;
    private Array<Image> slides;
    private ImageButton buttonBack, buttonNext, buttonBackToMenu;
    private int currentSlide;
    private StateOfTransicion stateOfTransicion;

    private enum StateOfTransicion {
        GOING_BACK, GOING_NEXT, STOP
    }

    public TutorialScreen(Game game) {

        super(game);
    }


    @Override
    public void show() {

        //==================================
        //      INITIALIZING VARIABLES
        //==================================


        currentSlide = 0;
        float padding = Gdx.graphics.getWidth() / 100f;

        stage = new Stage();

        //Enable input system
        Gdx.input.setInputProcessor(stage);

        //Upload slides in memory
        slides = new Array<Image>();
        Array<TextureAtlas.AtlasRegion> regionsOfSlides = Assets.instance.getTextureAtlasMenu().findRegions(Constants.TUTORIAL_SLIDES);

        for (TextureAtlas.AtlasRegion regionSlide : regionsOfSlides) {
            slides.add(new Image(regionSlide));
        }

        // Style and Textures
        TextureRegionDrawable textureBack = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.TUTORIAL_BACK));
        TextureRegionDrawable textureNext = new TextureRegionDrawable(Assets.instance.getTextureAtlasMenu().findRegion(Constants.TUTORIAL_NEXT));
        TextureRegionDrawable textureBackToMenuUp = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_BACK_MENU_UP));
        TextureRegionDrawable textureBackToMenuDown = new TextureRegionDrawable(Assets.instance.getTextureAtlasGame().findRegion(Constants.REFERENCE_NAME_ATLAS_BUTTON_BACK_MENU_DOWN));

        // Widgets
        buttonBack = new ImageButton(textureBack);
        buttonNext = new ImageButton(textureNext);
        buttonBackToMenu = new ImageButton(textureBackToMenuUp, textureBackToMenuDown);


        //==================================
        //  CONFIGURING TABLES AND WIDGETS
        //==================================

        //Slides
        for (Image imageCurrentSlide : slides) {

            imageCurrentSlide.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            imageCurrentSlide.setPosition(Gdx.graphics.getWidth(), 0);

        }

        //Just the first Slide is in screen
        slides.get(currentSlide).setX(0);


        //Buttons

        buttonBackToMenu.setSize(Gdx.graphics.getWidth() / 4f, Gdx.graphics.getHeight() / 7f);
        buttonBackToMenu.setPosition(Gdx.graphics.getWidth() / 2f - buttonBackToMenu.getWidth() / 2f, padding * 3);
        buttonBackToMenu.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game));
                super.clicked(event, x, y);
            }
        });

        buttonBack.setSize(Gdx.graphics.getWidth() / 6f, Gdx.graphics.getHeight() / 7f);
        buttonBack.setPosition(buttonBackToMenu.getX() - buttonBack.getWidth() - padding * 2, padding * 3);
        buttonBack.setVisible(false);
        buttonBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {

                if (stateOfTransicion.equals(StateOfTransicion.STOP)) {
                    stateOfTransicion = StateOfTransicion.GOING_BACK;
                    currentSlide--;

                    if (currentSlide == 0) {
                        buttonBack.setVisible(false);
                    }

                    if (!buttonNext.isVisible()) {
                        buttonNext.setVisible(true);
                    }
                }


                super.clicked(event, x, y);
            }
        });

        buttonNext.setSize(Gdx.graphics.getWidth() / 6, Gdx.graphics.getHeight() / 7);
        buttonNext.setPosition(buttonBackToMenu.getX() + buttonBackToMenu.getWidth() + padding * 2, padding * 3);
        buttonNext.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (stateOfTransicion.equals(StateOfTransicion.STOP)) {
                    stateOfTransicion = StateOfTransicion.GOING_NEXT;
                    currentSlide++;

                    if (currentSlide + 1 == slides.size) {
                        buttonNext.setVisible(false);
                    }

                    if (!buttonBack.isVisible()) {
                        buttonBack.setVisible(true);
                    }

                }

                super.clicked(event, x, y);
            }
        });


        //==================================
        //         ADDING ACTORS
        //==================================

        for (Image imageCurrentSlide : slides) {
            stage.addActor(imageCurrentSlide);
        }

        stage.addActor(buttonBackToMenu);
        stage.addActor(buttonBack);
        stage.addActor(buttonNext);

        stateOfTransicion = StateOfTransicion.STOP;


    }


    @Override
    public void hide() {


    }

    @Override
    public void render(float delta) {

        update(delta);

        stage.draw();
    }


    private void update(float delta) {


        float amount = delta * Constants.VELOCITY_PANELS;

        if (Gdx.app.getType().equals(Application.ApplicationType.Android)) {
            amount *= 2;
        }

        if (stateOfTransicion.equals(StateOfTransicion.GOING_NEXT)) {

            if (currentSlide < slides.size) {
                Image imagePostSlide = slides.get(currentSlide);
                if (imagePostSlide.getX() - amount < 0) {
                    imagePostSlide.setX(0);
                } else {
                    imagePostSlide.setX(imagePostSlide.getX() - amount);
                }

            }

            Image imageCurrentSlide = slides.get(currentSlide - 1);

            if (imageCurrentSlide.getX() - amount < -imageCurrentSlide.getWidth()) {
                imageCurrentSlide.setX(-imageCurrentSlide.getWidth());
                stateOfTransicion = StateOfTransicion.STOP;
            } else {
                imageCurrentSlide.setX(imageCurrentSlide.getX() - amount);
            }


        } else if (stateOfTransicion.equals(StateOfTransicion.GOING_BACK)) {

            if (currentSlide >= 0) {
                Image imagePrevSlide = slides.get(currentSlide + 1);

                if (imagePrevSlide.getX() + amount > Gdx.graphics.getWidth()) {
                    imagePrevSlide.setX(Gdx.graphics.getWidth());
                } else {
                    imagePrevSlide.setX(imagePrevSlide.getX() + amount);
                }
            }
            Image imageCurrentSlide = slides.get(currentSlide);

            if (imageCurrentSlide.getX() + amount > 0) {
                imageCurrentSlide.setX(0);
                stateOfTransicion = StateOfTransicion.STOP;
            } else {
                imageCurrentSlide.setX(imageCurrentSlide.getX() + amount);
            }


        }

    }


}