package org.raretaldefense.warofelements.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.hiero.Hiero;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

import org.raretaldefense.warofelements.game.Constants.Constants;
import org.raretaldefense.warofelements.game.RaretalDefenseMain;


public class DesktopLauncher {


    public static void main(String[] arg) {

        if (Constants.rebuildAtlasGame) {
            Settings settings = new Settings();
            settings.maxWidth = 2048;
            settings.maxHeight = 2048;
            settings.duplicatePadding = true;
            settings.debug = Constants.linesDebugAtlas;
            TexturePacker.process(settings,
                    Constants.PATH_ORIGIN_ATLAS,
                    Constants.PATH_DESTINATION_ATLAS,
                    Constants.NAME_PACKAGE_ATLAS);
        }

        if (Constants.rebuildAtlasMenu) {
            Settings settings = new Settings();
            settings.maxWidth = 4096;
            settings.maxHeight = 4096;
            settings.duplicatePadding = true;
            settings.debug = Constants.linesDebugAtlas;
            TexturePacker.process(settings,
                    Constants.PATH_ORIGIN_ATLAS_MENU,
                    Constants.PATH_DESTINATION_ATLAS_MENU,
                    Constants.NAME_PACKAGE_ATLAS_MENU);
        }
        if (Constants.runHiero) {
            try {
                Hiero.main(new String[]{});
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
            config.resizable = false;
            config.fullscreen = true;
//            config.height = 890;
//            config.width = 1024;
            config.title = Constants.DESKTOP_NAME_GAME;
            new LwjglApplication(new RaretalDefenseMain(), config);
        }
    }
}
